<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package   theme_adaptable
 * @copyright 2015-2021 Fernando Acedo (3-bits.com)
 * @copyright 2020-2021 3bits development team (3-bits.com)
 * @copyright 2015-2019 Jeremy Hopkins (Coventry University)
 * @copyright 2017-2019 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

// Constants.
define('THEME_ADAPTABLE_DEFAULT_ALERTCOUNT', '1');
define('THEME_ADAPTABLE_DEFAULT_ANALYTICSCOUNT', '1');
define('THEME_ADAPTABLE_DEFAULT_TOPMENUSCOUNT', '1');
define('THEME_ADAPTABLE_DEFAULT_TOOLSMENUSCOUNT', '1');
define('THEME_ADAPTABLE_DEFAULT_NEWSTICKERCOUNT', '1');
define('THEME_ADAPTABLE_DEFAULT_SLIDERCOUNT', '3');

/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */
function theme_adaptable_process_css($css, $theme) {

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_adaptable_set_customcss($css, $customcss);

    // Define the default settings for the theme in case they've not been set.
    $defaults = array(
        '[[setting:alertbackgroundcolorinfo]]' => '#3bb6ef',
        '[[setting:alertbackgroundcolorsuccess]]' => '#4eb52f',
        '[[setting:alertbackgroundcolorwarning]]' => '#ffda21',
        '[[setting:alertbordercolorinfo]]' => '#3bb6ef',
        '[[setting:alertbordercolorsuccess]]' => '#4eb52f',
        '[[setting:alertbordercolorwarning]]' => '#ffda21',
        '[[setting:alertcolorinfo]]' => '#1d6cac',
        '[[setting:alertcolorsuccess]]' => '#208217',
        '[[setting:alertcolorwarning]]' => '#b79510',
        '[[setting:alertcount]]' => 1,
        '[[setting:backcolor]]' => '#fff',
        '[[setting:blockbackgroundcolor]]' => '#fff',
        '[[setting:blockbordercolor]]' => '#59585d',
        '[[setting:blockheaderbackgroundcolor]]' => '#fff',
        '[[setting:blockheaderborderbottom]]' => '0px',
        '[[setting:blockheaderborderleft]]' => '0px',
        '[[setting:blockheaderborderright]]' => '0px',
        '[[setting:blockheaderbordertop]]' => '1px',
        '[[setting:blockheaderbordertopstyle]]' => 'solid',
        '[[setting:blockheaderbottomradius]]' => '0px',
        '[[setting:blockheadertopradius]]' => '0px',
        '[[setting:blockiconsheadersize]]' => '2rem',
        '[[setting:blockmainborderbottom]]' => '0px',
        '[[setting:blockmainborderleft]]' => '0px',
        '[[setting:blockmainborderright]]' => '0px',
        '[[setting:blockmainbordertop]]' => '0px',
        '[[setting:blockmainbordertopstyle]]' => 'solid',
        '[[setting:blockmainbottomradius]]' => '0px',
        '[[setting:blockmaintopradius]]' => '0px',
        '[[setting:blockregionbackgroundcolor]]' => 'transparent',
        '[[setting:breadcrumb]]' => '#b4bbbf',
        '[[setting:breadcrumbseparator]]' => 'angle-right',
        '[[setting:breadcrumbtextcolor]]' => '#444',
        '[[setting:buttoncancelbackgroundcolor]]' => '#d8d5d5',
        '[[setting:buttoncancelcolor]]' => '#1d1c1c',
        '[[setting:buttoncolor]]' => '#ca0170',
        '[[setting:buttondropshadow]]' => '0px',
        '[[setting:buttonhovercolor]]' => '#91006d',
        '[[setting:buttonlogincolor]]' => '#ef5350',
        '[[setting:buttonloginheight]]' => '24px',
        '[[setting:buttonloginhovercolor]]' => '#e53935',
        '[[setting:buttonloginmargintop]]' => '2px',
        '[[setting:buttonloginpadding]]' => '0px',
        '[[setting:buttonlogintextcolor]]' => '#0084c2',
        '[[setting:buttonradius]]' => '5px',
        '[[setting:buttontextcolor]]' => '#fff',
        '[[setting:coursesectionactivityassignbgcolor]]' => '#fff',
        '[[setting:coursesectionactivityassignleftbordercolor]]' => '#0170ca',
        '[[setting:coursesectionactivitybordercolor]]' => '#ccc',
        '[[setting:coursesectionactivityborderstyle]]' => 'solid',
        '[[setting:coursesectionactivityborderwidth]]' => '2px',
        '[[setting:coursesectionactivityforumbgcolor]]' => '#fff',
        '[[setting:coursesectionactivityforumleftbordercolor]]' => '#909',
        '[[setting:coursesectionactivityheadingcolour]]' => '#0170ca',
        '[[setting:coursesectionactivityiconsize]]' => '24px',
        '[[setting:coursesectionactivityleftborderwidth]]' => '3px',
        '[[setting:coursesectionactivitymarginbottom]]' => '2px',
        '[[setting:coursesectionactivitymargintop]]' => '2px',
        '[[setting:coursesectionactivityquizbgcolor]]' => '#fff',
        '[[setting:coursesectionactivityquizleftbordercolor]]' => '#f33',
        '[[setting:coursesectionbgcolor]]' => '#fff',
        '[[setting:coursesectionbordercolor]]' => '#eee',
        '[[setting:coursesectionborderradius]]' => '0px',
        '[[setting:coursesectionborderstyle]]' => '1px',
        '[[setting:coursesectionborderwidth]]' => '2px',
        '[[setting:coursesectionheaderbg]]' => '#eee',
        '[[setting:coursesectionheaderbordercolor]]' => '#f3f3f3',
        '[[setting:coursesectionheaderborderradiusbottom]]' => '',
        '[[setting:coursesectionheaderborderradiustop]]' => '',
        '[[setting:coursesectionheaderborderstyle]]' => 'solid',
        '[[setting:coursesectionheaderborderwidth]]' => '2px',
        '[[setting:coursetitlemaxwidth]]' => '20',
        '[[setting:coursetitlepaddingleft]]' => '0px',
        '[[setting:coursetitlepaddingtop]]' => '0px',
        '[[setting:coursetitlepaddingtop]]' => '0px',
        '[[setting:covbkcolor]]' => '#3a454b',
        '[[setting:covfontcolor]]' => '#fff',
        '[[setting:currentcolor]]' => '#d9edf7',
        '[[setting:customfontheadername]]' => '',
        '[[setting:customfontname]]' => '',
        '[[setting:customfonttitlename]]' => '',
        '[[setting:dangericoncolor]]' => '#d9534f',
        '[[setting:disablecustommenu]]' => 0,
        '[[setting:dividingline]]' => '#fff',
        '[[setting:dividingline2]]' => '#fff',
        '[[setting:editfont]]' => '#fff',
        '[[setting:edithorizontalpadding]]' => '',
        '[[setting:editoffbk]]' => '#f44336',
        '[[setting:editonbk]]' => '#4caf50',
        '[[setting:edittopmargin]]' => '',
        '[[setting:editsettingsbutton]]' => 'cogandbutton',
        '[[setting:editverticalpadding]]' => '',
        '[[setting:emoticonsize]]' => '16px',
        '[[setting:enableavailablecourses]]' => 'none',
        '[[setting:enablemenus]]' => 'false',
        '[[setting:enablesavecanceloverlay]]' => 'false',
        '[[setting:enableshowhideblocks]]' => 1,
        '[[setting:enableshowhideblockstext]]' => 0,
        '[[setting:enableticker]]' => 'true',
        '[[setting:enabletickermy]]' => 'false',
        '[[setting:enabletoolsmenus]]' => 0,
        '[[setting:focusborder]]' => '#0170ca',
        '[[setting:focusbackground]]' => '#d7ebfc',
        '[[setting:fontblockheadercolor]]' => '#0170ca',
        '[[setting:fontblockheadersize]]' => '1.5rem',
        '[[setting:fontblockheaderweight]]' => '400',
        '[[setting:fontcolor]]' => '#333',
        '[[setting:fontheadercolor]]' => '#333',
        '[[setting:fontheadername]]' => 'Archivo',
        '[[setting:fontheaderweight]]' => '400',
        '[[setting:fontname]]' => 'Lexend Deca',
        '[[setting:fontsize]]' => '1rem',
        '[[setting:fonttitlecolor]]' => '#fff',
        '[[setting:fonttitlecolorcourse]]' => '#fff',
        '[[setting:fonttitlename]]' => 'Lexend Giga',
        '[[setting:fonttitlesize]]' => '48px',
        '[[setting:fonttitleweight]]' => '400',
        '[[setting:fontweight]]' => '400',
        '[[setting:footerbkcolor]]' => '#424242',
        '[[setting:footerlinkcolor]]' => '#fff',
        '[[setting:footertextcolor]]' => '#fff',
        '[[setting:footertextcolor2]]' => '#fff',
        '[[setting:forumbodybackgroundcolor]]' => '#fff',
        '[[setting:forumheaderbackgroundcolor]]' => '#fff',
        '[[setting:frontpagenumbertiles]]' => '4',
        '[[setting:fullscreenwidth]]' => '98%',
        '[[setting:gdprbutton]]' => 1,
        '[[setting:headerbkcolor]]' => '#fff',
        '[[setting:headerbkcolor2]]' => '#0170ca',
        '[[setting:headertextcolor]]' => '#333',
        '[[setting:headertextcolor2]]' => '#fff',
        '[[setting:hidealertsmobile]]' => 0,
        '[[setting:hidebreadcrumbmobile]]' => 1,
        '[[setting:hidecoursetitlemobile]]' => 0,
        '[[setting:hidefootersocial]]' => 1,
        '[[setting:hideheadermobile]]' => 0,
        '[[setting:hidelogomobile]]' => 0,
        '[[setting:hidepagefootermobile]]' => 0,
        '[[setting:hideslidermobile]]' => '1',
        '[[setting:hidesocialmobile]]' => 0,
        '[[setting:infoiconcolor]]' => '#5bc0de',
        '[[setting:linkcolor]]' => '#607d8b',
        '[[setting:linkhover]]' => '#0170ca',
        '[[setting:loadingcolor]]' => '#33a3fe',
        '[[setting:loginfooter]]' => 1,
        '[[setting:loginheader]]' => 1,
        '[[setting:maincolor]]' => '#3a454b',
        '[[setting:marketblockbordercolor]]' => '#e8eaeb',
        '[[setting:marketblocksbackgroundcolor]]' => 'transparent',
        '[[setting:menubkcolor]]' => '#fff',
        '[[setting:menubordercolor]]' => '#33a3fe',
        '[[setting:menufontcolor]]' => '#444',
        '[[setting:menufontpadding]]' => '20px',
        '[[setting:menufontsize]]' => '0.8rem',
        '[[setting:menuhovercolor]]' => '#33a3fe',
        '[[setting:menuslinkicon]]' => 'fa-link',
        '[[setting:messagingbackgroundcolor]]' => '#fff',
        '[[setting:mobile]]' => '22',
        '[[setting:mobilemenubkcolor]]' => '#f9f9f9',
        '[[setting:mobilemenufontcolor]]' => '#000',
        '[[setting:mobileslidebartabbkcolor]]' => '#f9f9f9',
        '[[setting:mobileslidebartabiconcolor]]' => '#000',
        '[[setting:msgbadgecolor]]' => '#e53935',
        '[[setting:msgbadgecolortext]]' => '#fff',
        '[[setting:navbardropdownborderradius]]' => '0px',
        '[[setting:navbardropdownhovercolor]]' => '#eee',
        '[[setting:navbardropdowntransitiontime]]' => '0.2s',
        '[[setting:regionmaincolor]]' => '#fff',
        '[[setting:rendereroverlaycolor]]' => '#3a454b',
        '[[setting:rendereroverlayfontcolor]]' => '#fff',
        '[[setting:rendereroverlaytitlecolor]]' => '#0170ca',
        '[[setting:searchboxpadding]]' => '15px 0px 0px 0px',
        '[[setting:sectionheadingcolor]]' => '#333',
        '[[setting:selectionbackground]]' => '#33a3fe',
        '[[setting:selectiontext]]' => '#000',
        '[[setting:showyourprogress]]' => '',
        '[[setting:sidebarnotlogged]]' => 'true',
        '[[setting:sitetitlemaxwidth]]' => '50%',
        '[[setting:sitetitlepaddingleft]]' => '0px',
        '[[setting:sitetitlepaddingtop]]' => '0px',
        '[[setting:slider2h3bgcolor]]' => '#000',
        '[[setting:slider2h3color]]' => '#000',
        '[[setting:slider2h4bgcolor]]' => '#fff',
        '[[setting:slider2h4color]]' => '#000',
        '[[setting:slidercount]]' => 3,
        '[[setting:sliderh3color]]' => '#fff',
        '[[setting:sliderh4color]]' => '#fff',
        '[[setting:slidermarginbottom]]' => '20px',
        '[[setting:slidermargintop]]' => '20px',
        '[[setting:slideroption2a]]' => '#607d8b',
        '[[setting:slideroption2color]]' => '#607d8b',
        '[[setting:slideroption2submitcolor]]' => '#fff',
        '[[setting:slidersubmitbgcolor]]' => '#607d8b',
        '[[setting:slidersubmitcolor]]' => '#fff',
        '[[setting:socialpaddingside]]' => '16',
        '[[setting:socialpaddingtop]]' => '0%',
        '[[setting:socialsize]]' => '37px',
        '[[setting:socialsizemobile]]' => '34px',
        '[[setting:socialsearch]]' => '',
        '[[setting:socialtarget]]' => '_blank',
        '[[setting:socialwallactionlinkcolor]]' => '#607d8b',
        '[[setting:socialwallactionlinkhovercolor]]' => '#0170ca',
        '[[setting:socialwallbackgroundcolor]]' => '#fff',
        '[[setting:socialwallbordercolor]]' => '#b9b9b9',
        '[[setting:socialwallbordertopstyle]]' => 'solid',
        '[[setting:socialwallborderwidth]]' => '2px',
        '[[setting:socialwallsectionradius]]' => '6px',
        '[[setting:tabbedlayoutcoursepagetabcolorselected]]' => '#0170ca',
        '[[setting:tabbedlayoutcoursepagetabcolorunselected]]' => '#eee',
        '[[setting:tabbedlayoutdashboardcolorselected]]' => '#0170ca',
        '[[setting:tabbedlayoutdashboardcolorunselected]]' => '#eee',
        '[[setting:tilesbordercolor]]' => '#3a454b',
        '[[setting:buttonsearchcolor]]' => '#ca0170',
        '[[setting:buttonsearchhovercolor]]' => '#91006d',
        '[[setting:buttonsearchtextcolor]]' => '#fff',
        '[[setting:messagesiconscolor]]' => '0%',
        '[[setting:topmenufontsize]]' => '0.8rem'
    );

    // Get all the defined settings for the theme and replace defaults.
    foreach ($theme->settings as $key => $val) {
        if (array_key_exists('[[setting:'.$key.']]', $defaults) && !empty($val)) {
            $defaults['[[setting:'.$key.']]'] = $val;
        }
    }

    // Site background image.
    $homebkg = '';
    if (!empty($theme->settings->homebk)) {
        $homebkg = $theme->setting_file_url('homebk', 'homebk');
        $homebkg = 'background-image: url("' . $homebkg . '");';
    }
    $defaults['[[setting:homebkg]]'] = $homebkg;

    // Login background image.
    $loginbgimage = '';
    if (!empty($theme->settings->loginbgimage)) {
        $loginbgimage = $theme->setting_file_url('loginbgimage', 'loginbgimage');
        $loginbgimage = 'background-image: url("' . $loginbgimage . '");';
    }
    $defaults['[[setting:loginbgimage]]'] = $loginbgimage;

    $socialpaddingsidehalf = '16';
    if (!empty($theme->settings->socialpaddingside)) {
        $socialpaddingsidehalf = ''.$theme->settings->socialpaddingside / 2;
    }
    $defaults['[[setting:socialpaddingsidehalf]]'] = $socialpaddingsidehalf;

    // Replace the CSS with values from the $defaults array.
    $css = strtr($css, $defaults);
    if (empty($theme->settings->tilesshowallcontacts) || $theme->settings->tilesshowallcontacts == false) {
        $css = theme_adaptable_set_tilesshowallcontacts($css, false);
    } else {
        $css = theme_adaptable_set_tilesshowallcontacts($css, true);
    }
    return $css;
}


/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_adaptable_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Set display of course contacts on front page tiles
 * @param string $css
 * @param string $display
 * @return $string
 */
function theme_adaptable_set_tilesshowallcontacts($css, $display) {
    $tag = '[[setting:tilesshowallcontacts]]';
    if ($display) {
        $replacement = 'block';
    } else {
        $replacement = 'none';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

/**
 * Get the user preference for the zoom (show / hide block) function.
 */
function theme_adaptable_get_zoom() {
    return get_user_preferences('theme_adaptable_zoom', '');
}

/**
 * Set user preferences for zoom (show / hide block) function
 * @param moodle_page $page
 * @return void
 */
function theme_adaptable_initialise_zoom(moodle_page $page) {
    user_preference_allow_ajax_update('theme_adaptable_zoom', PARAM_TEXT);
}

/**
 * Set the user preference for full screen
 * @param moodle_page $page
 * @return void
 */
function theme_adaptable_initialise_full(moodle_page $page) {
    user_preference_allow_ajax_update('theme_adaptable_full', PARAM_TEXT);
}

/**
 * Get the user preference for the zoom function.
 */
function theme_adaptable_get_full() {
    return get_user_preferences('theme_adaptable_full', '');
}

/**
 * Get the key of the last closed alert for a specific alert index.
 * This will be used in the renderer to decide whether to include the alert or not
 * @param int $alertindex
 */
function theme_adaptable_get_alertkey($alertindex) {
    user_preference_allow_ajax_update('theme_adaptable_alertkey' . $alertindex, PARAM_TEXT);
    return get_user_preferences('theme_adaptable_alertkey' . $alertindex, '');
}

/**
 * Get HTML for settings
 * @param renderer_base $output
 * @param moodle_page $page
 */
function theme_adaptable_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote">'.$page->theme->settings->footnote.'</div>';
    }

    return $return;
}

/**
 * Get theme setting
 * @param string $setting
 * @param string $format = false
 */
function theme_adaptable_get_setting($setting, $format = false) {
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('adaptable');
    }

    if (empty($theme->settings->$setting)) {
        return false;
    } else if (!$format) {
        return $theme->settings->$setting;
    } else if ($format === 'format_text') {
        return format_text($theme->settings->$setting, FORMAT_PLAIN);
    } else if ($format === 'format_html') {
        return format_text($theme->settings->$setting, FORMAT_HTML, array('trusted' => true));
    } else {
        return format_string($theme->settings->$setting);
    }
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_adaptable_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('adaptable');
    }
    if ($context->contextlevel == CONTEXT_SYSTEM) {
        if ($filearea === 'logo') {
            return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
        } else if ($filearea === 'favicon') {
            return $theme->setting_file_serve('favicon', $args, $forcedownload, $options);
        } else if ($filearea === 'homebk') {
            return $theme->setting_file_serve('homebk', $args, $forcedownload, $options);
        } else if ($filearea === 'pagebackground') {
            return $theme->setting_file_serve('pagebackground', $args, $forcedownload, $options);
        } else if (preg_match("/^p[1-9][0-9]?$/", $filearea) !== false) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if ((substr($filearea, 0, 9) === 'marketing') && (substr($filearea, 10, 5) === 'image')) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if ($filearea === 'iphoneicon') {
            return $theme->setting_file_serve('iphoneicon', $args, $forcedownload, $options);
        } else if ($filearea === 'iphoneretinaicon') {
            return $theme->setting_file_serve('iphoneretinaicon', $args, $forcedownload, $options);
        } else if ($filearea === 'ipadicon') {
            return $theme->setting_file_serve('ipadicon', $args, $forcedownload, $options);
        } else if ($filearea === 'ipadretinaicon') {
            return $theme->setting_file_serve('ipadretinaicon', $args, $forcedownload, $options);
        } else if ($filearea === 'fontfilettfheading') {
            return $theme->setting_file_serve('fontfilettfheading', $args, $forcedownload, $options);
        } else if ($filearea === 'fontfilettfbody') {
            return $theme->setting_file_serve('fontfilettfbody', $args, $forcedownload, $options);
        } else if ($filearea === 'adaptablemarkettingimages') {
            return $theme->setting_file_serve('adaptablemarkettingimages', $args, $forcedownload, $options);
        } else {
            send_file_not_found();
        }
    } else {
        send_file_not_found();
    }
}

/**
 * Get course activities for this course menu
 */
function theme_adaptable_get_course_activities() {
    GLOBAL $CFG, $PAGE, $OUTPUT;
    // A copy of block_activity_modules.
    $course = $PAGE->course;
    $content = new stdClass();
    $modinfo = get_fast_modinfo($course);
    $modfullnames = array();

    $archetypes = array();

    foreach ($modinfo->cms as $cm) {
        // Exclude activities which are not visible or have no link (=label).
        if (!$cm->uservisible or !$cm->has_view()) {
            continue;
        }
        if (array_key_exists($cm->modname, $modfullnames)) {
            continue;
        }
        if (!array_key_exists($cm->modname, $archetypes)) {
            $archetypes[$cm->modname] = plugin_supports('mod', $cm->modname, FEATURE_MOD_ARCHETYPE, MOD_ARCHETYPE_OTHER);
        }
        if ($archetypes[$cm->modname] == MOD_ARCHETYPE_RESOURCE) {
            if (!array_key_exists('resources', $modfullnames)) {
                $modfullnames['resources'] = get_string('resources');
            }
        } else {
            $modfullnames[$cm->modname] = $cm->modplural;
        }
    }
    core_collator::asort($modfullnames);

    return $modfullnames;
}

/**
 * Get formatted performance info showing only page load time
 * @param string $param
 */
function theme_adaptable_performance_output($param) {
    $html = html_writer::tag('span', get_string('loadtime', 'theme_adaptable').' '. round($param['realtime'], 2) . ' ' .
            get_string('seconds'), array('id' => 'load'));
    return $html;
}

/**
 * Initialize page
 * @param moodle_page $page
 */
function theme_adaptable_page_init(moodle_page $page) {
    global $CFG;

    $page->requires->jquery_plugin('pace', 'theme_adaptable');
    $page->requires->jquery_plugin('flexslider', 'theme_adaptable');
    $page->requires->jquery_plugin('ticker', 'theme_adaptable');
    $page->requires->jquery_plugin('easing', 'theme_adaptable');
    $page->requires->jquery_plugin('adaptable', 'theme_adaptable');

}

/**
 * Strip full site title from header
 * @param string $heading
 */
function theme_adaptable_remove_site_fullname($heading) {
    global $SITE, $PAGE;
    if (strpos($PAGE->pagetype, 'course-view-') === 0) {
        return $heading;
    }

    $header = preg_replace("/^".$SITE->fullname."/", "", $heading);

    return $header;
}

/**
 * Generate theme grid.
 * @param bool $left
 * @param bool $hassidepost
 */
function theme_adaptable_grid($left, $hassidepost) {
    if ($hassidepost) {
        if ('rtl' === get_string('thisdirection', 'langconfig')) {
            $left = !$left; // Invert.
        }
        $regions = array('content' => 'col-9');
        $regions['blocks'] = 'col-3';
        if ($left) {
            $regions['direction'] = ' flex-row-reverse';
        } else {
            $regions['direction'] = '';
        }
    } else {
        $regions = array('content' => 'col-12');
        $regions['direction'] = '';
        return $regions;
    }

    return $regions;
}

/**
 * Detect device.
 */
function theme_adaptable_is_desktop() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    return stripos($useragent, 'mobile') === false &&
        stripos($useragent, 'tablet') === false && stripos($useragent, 'ipad') === false;
}

function theme_adaptable_is_tablet() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    return stripos($useragent, 'tablet') !== false || stripos($useragent, 'tab') !== false;
}

function theme_adaptable_is_ipad() {
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    return stripos($useragent, 'ipad') !== false;
}

function theme_adaptable_is_mobile() {
    $ismobile = false;
    if (empty($_SERVER['HTTP_USER_AGENT'])) {
        return false;
    } else if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Mobile' ) !== false // Many mobile devices (all iPhone, iPad, etc.).
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'Android' ) !== false
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'Silk/' ) !== false
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'Kindle' ) !== false
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'BlackBerry' ) !== false
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'Opera Mini' ) !== false
        || strpos( $_SERVER['HTTP_USER_AGENT'], 'Opera Mobi' ) !== false ) {
            $ismobile = true;
    } else {
        $ismobile = false;
    }
    return $ismobile;
}


/**
 *
 * Get the current page to allow us to check if the block is allowed to display.
 *
 * @return string The page name, which is either "frontpage", "dashboard", "coursepage", "coursesectionpage" or empty string.
 *
 */
function theme_adaptable_get_current_page() {
    global $COURSE, $PAGE;

    // This will store the kind of activity page type we find. E.g. It will get populated with 'section' or similar.
    $currentpage = '';

    // We expect $PAGE->url to exist.  It should!
    $url = $PAGE->url;

    if ($PAGE->pagetype == 'site-index') {
        $currentpage = 'frontpage';
    } else if ($PAGE->pagetype == 'my-index') {
        $currentpage = 'dashboard';
    }
    // Check if course home page.
    if (empty ($currentpage)) {
        if ($url !== null) {
            // Check if this is the course view page.
            if (strstr ($url->raw_out(), 'course/view.php')) {

                $currentpage = 'coursepage';

                // Get raw querystring params from URL.
                $getparams = http_build_query($_GET);

                // Check url paramaters.  Count should be 1 if course home page. Use this to check if section page.
                $urlparams = $url->params();

                // Allow the block to display on course sections too if the relevant setting is on.
                if ((count ($urlparams) > 1) && (array_key_exists('section', $urlparams))) {
                    $currentpage = 'coursesectionpage';
                }

            }
        }
    }

    return $currentpage;
}
