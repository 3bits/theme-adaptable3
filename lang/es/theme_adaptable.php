<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Spanish Translation
 *
 * @package    theme adaptable
 * @copyright  2015-2021 Fernando Acedo (3-bits.com)
 * @copyright 2020-2021 3bits development team (3-bits.com)
 *

 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * ********************************************************************************************************************************
 * Para evitar conflictos, NO USAR carácteres como comillas simples (') o dobles (") en el texto traducido.
 * Usar en su lugar entidades HTML. Ver https://dev.w3.org/html5/html-author/charref
 *
 * ' -> &#39; or &apos;
 * " -> &#34; or &quote;
 *
 * Ejemplo: "HOLA" -> &#34;Hola&#34;
 ***********************************************************************************************************************************/

// General.
$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Adaptable</h2>
<p><img class="img-polaroid" src="adaptable/pix/screenshot.png" /></p>
</div>
<div class="well">
<h3>Acerca de...</h3>
<p>Adaptable es una plantilla para Moodle, de dos columnas y tipo responsive, altamente personalizable. La versión 3 utiliza Boost como base y Bootstrap 4 como framework CSS.</p>
<p>Esta plantilla está bajo licencia GPL (GNU General Public License). Puedes encontrar la licencia completa en: <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses</a></p>
<br>
<p>¡Modifícala! / ¡Mejórala! / ¡Compártela!</p>
<br>
<h3>Créditos</h3>
<p>Este tema ha sido creada por:<br>
Fernando Acedo (<a href="https://3-bits.com" target="_blank">3-bits.com</a>)<br />
3bits development team<br />
</p>
<p>Y anteriormente por:<br />
Jeremy Hopkins (Coventry University)<br />
Manoj Solanki (Coventry University)<br />
<br />
<br />
<p>entre otros muchos desarrolladores, probadores, traductores y voluntarios</p>
<br>
<p>Puedes informar de errores (y por favor, <b>SOLO</b> errores) en <a href="https://adaptable.ws" target="_blank">nuestro sitio de soporte</a></p>
<br>
<br>
<h3>Sitio de Demostración</h3>
<p>Puedes ver <a href="https://adaptable.ws/demo" target="_blank">una demostración del tema AQUÍ</a></p>
</div>
</div>';

$string['pluginname'] = 'Adaptable';
$string['configtitle'] = 'Adaptable';

$string['region-middle'] = 'Centro';
$string['region-frnt-footer'] = 'Pie';
$string['region-side-post'] = 'Derecha';
$string['region-side-pre'] = 'Izquierda';
$string['frnt-footer'] = 'Los bloques situados en esta regi&Oacute;n solo son visibles para los administradores.';
$string['side-post1'] = 'Barra lateral en el pie de página ';

$string['region-frnt-market-a'] = 'Región-a';
$string['region-frnt-market-b'] = 'Región-b';
$string['region-frnt-market-c'] = 'Región-c';
$string['region-frnt-market-d'] = 'Región-d';
$string['region-frnt-market-e'] = 'Región-e';
$string['region-frnt-market-f'] = 'Región-f';
$string['region-frnt-market-g'] = 'Región-g';
$string['region-frnt-market-h'] = 'Región-h';
$string['region-frnt-market-i'] = 'Región-i';
$string['region-frnt-market-j'] = 'Región-j';
$string['region-frnt-market-k'] = 'Región-k';
$string['region-frnt-market-l'] = 'Región-l';
$string['region-frnt-market-m'] = 'Región-m';
$string['region-frnt-market-n'] = 'Región-n';
$string['region-frnt-market-o'] = 'Región-o';
$string['region-frnt-market-p'] = 'Región-p';
$string['region-frnt-market-q'] = 'Región-q';
$string['region-frnt-market-r'] = 'Región-r';
$string['region-frnt-market-s'] = 'Región-s';
$string['region-frnt-market-t'] = 'Región-t';

// Course page block regions.
$string['region-course-top-a'] = 'Región superior página curso 1';
$string['region-course-top-b'] = 'Región superior página curso 2';
$string['region-course-top-c'] = 'Región superior página curso 3';
$string['region-course-top-d'] = 'Región superior página curso 4';

$string['region-news-slider-a'] = 'Región para presentación de diapositivas de cursos';

$string['region-course-section-a'] = 'Región inferior de la pagina de actividad del curso';

$string['region-course-bottom-a'] = 'Región Inferior Página Curso 5';
$string['region-course-bottom-b'] = 'Región Inferior Página Curso 6';
$string['region-course-bottom-c'] = 'Región Inferior Página Curso 7';
$string['region-course-bottom-d'] = 'Región Inferior Página Curso 8';

// Settings page headings ******************************************.
$string['settingsabout'] = 'Acerca de ...';

$string['settingscolors'] = 'Colores';
$string['settingsmaincolors'] = 'Colores principales';
$string['settingsheadercolors'] = 'Colores cabecera';
$string['settingsmobilecolors'] = 'Colores para móvil';
$string['settingsformscolors'] = 'Colores formularios';
$string['settingsothercolors'] = 'Otros colores';
$string['settingsmenucolors'] = 'Colores menú';
$string['settingsmobilemenucolors'] = 'Colores menú móvil';
$string['settingsmarketingcolors'] = 'Colores bloques promocionales';
$string['settingsoverlaycolors'] = 'Colores bloques cursos';
$string['settingsnavbarcolors'] = 'Colores de la barra de navegación';
$string['settingsalertbox'] = 'Alertas';
$string['settingsbreadcrumbcolors'] = 'Colores de la ruta de navegación';
$string['settingsmessagescolors'] = 'Colores de los mensajes emergentes';
$string['settingsfootercolors'] = 'Colores del pie de página';
$string['settingsforumcolors'] = 'Colores foro';
$string['settingsaccesscolors'] = 'Colores accesibilidad';

$string['buttonsettings'] = 'Botones';
$string['settingscommonbuttons'] = 'Configuración común';
$string['settingscolorsbuttons'] = 'Colores generales';
$string['settingsloginbutton'] = 'Botón de Entrada';
$string['settingscancelbutton'] = 'Botón Cancelar';

$string['fontsettings'] = 'Fuentes';
$string['fontsettingsheading'] = 'Establece las fuentes utilizadas por la plantilla.';
$string['fontdesc'] = 'Selecciona la fuente de <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a> para el texto, los encabezamientos y el título. Selecciona también el juego de caracteres a usar (latín está incluido por defecto) e introduce el valor correcto para el grueso de fuente utilizado o no se mostrará.';
$string['settingsmainfont'] = 'Principal';
$string['settingsheaderfont'] = 'Cabeceras';
$string['settingstitlefont'] = ' Nombre del sitio / Título del curso';
$string['settingsmenufont'] = 'Menus';

$string['analyticssettings'] = 'Analítica Web';
$string['settingsborders'] = 'Bordes';
$string['settingstopicsweeks'] = 'Formato Temas / Semanas';
$string['settingsblockicons'] = 'Iconos';

// Admin Menu Strings.
$string['blocksettings'] = 'Bloques';
$string['frontpagealertsettings'] = 'Alertas';
$string['frontpageblockregionsettings'] = 'Constructor de regiones para bloques';
$string['dashboardblockregionsettings'] = 'Constructor de regiones del Área Personal';
$string['coursepageblockregionsettings'] = 'Constructor de regiones para el Curso';
$string['frontpageblocksettings'] = 'Bloques promocionales';
$string['frontpagetickersettings'] = 'Anuncios';
$string['frontpageslidersettings'] = 'Presentación diapositivas';
$string['frontpagecoursesettings'] = 'Cursos';
$string['frontpagesettingsheading'] = 'Página Inicial';
$string['frontpagedesc'] = 'Configurar la forma en que los cursos son mostrados en la página inicial.';
$string['frontpagerenderer'] = 'Bloques de cursos';
$string['frontpagerendererdesc'] = 'Configurar la forma en que los cursos son mostrados en la página inicial.';
$string['frontpagerendereroption1'] = 'Bloques';
$string['frontpagerendereroption2'] = 'Bloques sin incrustar';
$string['frontpagerendereroption3'] = 'Por defecto';


// Ticker **********************************************************.
$string['tickersettings'] = 'Anuncios';
$string['tickersettingsheading'] = 'Configura los anuncios de la página de inicio. Ver posición <a href="../theme/adaptable/pix/layout.png" target="_blank"> aquí</a>';
$string['tickerdesc'] = 'Aquí puedes configurar los anuncios de la página de inicio de tu moodle. Debes introducir una lista HTML con una linea para cada anuncio. Puedes incluir enlaces si lo precisas.<br />
    <strong>Nota:</strong> Cambia el editor a modo HTML y comprueba que la estructura corresponde a la indicada a continuación:
    <pre>
    &lt;ul&gt;
    &lt;li&gt;Anuncio Uno.....&lt;/li&gt;
    &lt;li&gt;Anuncio dos.....&lt;/li&gt;
    &lt;li&gt;Anuncio tres.....&lt;/li&gt;
    &lt;/ul&gt;
    </pre>';

$string['tickerdefault'] = 'No hay anuncios para mostrar';

$string['enableticker'] = 'Activar los anuncios en la página de inicio';
$string['enabletickerdesc'] = 'Marcar para activar los anuncios en la página de inicio.';

$string['enabletickermy'] = 'Activar los anuncios en Mi Inicio / Área Personal';
$string['enabletickermydesc'] = 'Marcar para activar los anuncios en la página de Mi Inicio / Área Personal.';

$string['enabletickerc'] = 'Activar los anuncios en las páginas internas';
$string['enabletickercdesc'] = 'Marcar para activar los anuncios en las páginas internas.';




$string['tickertextprofilefield'] = 'Nombre del campo personalizado del perfíl Field Name=Value (opcional)';
$string['tickertextprofilefielddesc'] = 'Añade una regla de acceso usando un campo de perfil personalizado. Ejemplo: usertype=alumno';

$string['ticker'] = 'Anuncios';

$string['tickerfixedscreen'] = 'Ancho fijo';
$string['tickerfullscreen'] = 'Ancho pantalla completa';

// Slideshow *******************************************************.
$string['slideshowsettings'] = 'Presentación de diapositivas';
$string['slideshowsettingsheading'] = 'Personaliza la presentación de diapositivas de la página de inicio';
$string['slideshowdesc'] = 'Sube imágenes, añade enlaces y descripciones en las dispositivas de la presentación de la página de inicio.';

$string['sliderimage'] = 'Imagen de la diapositiva';
$string['sliderimagedesc'] = 'Añade una imagen a la diapositiva.';

$string['slidercaption'] = 'Texto de la diapositiva';
$string['slidercaptiondesc'] = 'Añade un texto a la diapositiva.';

$string['sliderurl'] = 'URL de la diapositiva';
$string['sliderurldesc'] = 'Añade una URL a enlazar desde la diapositiva.';

$string['slidermargintop'] = 'Margen superior de la presentación de diapositivas';
$string['slidermargintopdesc'] = 'Establece el margen superior de la presentación de diapositivas.';

$string['slidermarginbottom'] = 'Margen inferior de la presentación de diapositivas';
$string['slidermarginbottomdesc'] = 'Establece el margen inferior de la presentación de diapositivas.';

$string['sliderenabled'] = 'Activar la presentación de diapositivas';
$string['sliderenableddesc'] = 'Activa la presentación de diapositivas en la página principal.';

$string['sliderfullscreen'] = 'Presentación de diapositivas en ancho completo';
$string['sliderfullscreendesc'] = 'Muestra la presentación de diapositivas en todo el ancho de la pantalla.';

$string['slideroption2'] = 'Seleccionar tipo de presentación de diapositivas';
$string['slideroption2desc'] = 'Selecciona el tipo de presentación de diapositivas <strong>y pulsar Guardar</strong>para mostrar los colores del estilo seleccionado.';

$string['slideroption2snippet'] = '<p>Ejemplo HTML para el texto de las diapositivas:</p>
<pre>
&#x3C;div class=&#x22;span6 col-sm-6&#x22;&#x3E;
&#x3C;h3&#x3E;Hecho a mano&#x3C;/h3&#x3E; &#x3C;h4&#x3E;pixels y código para la comunidad Moodle&#x3C;/h4&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;submit&#x22;&#x3E;Por favor, haz Adaptable uno de tus temas favoritos&#x3C;/a&#x3E;
</pre>';

$string['slidercount'] = 'Número de dispositivas';
$string['slidercountdesc'] = 'Establece el número de diapositivas a mostrar en la página principal y <strong>haz clic en Guardar</strong> para introducir la información del resto de campos.';

$string['sliderh3color'] = 'Color del texto H3 del estilo de presentación 1';
$string['sliderh3colordesc'] = 'Establece el color del texto de la etiqueta H3 cuando se utiliza el estilo de presentación 1.';

$string['sliderh4color'] = 'Color del texto H4 del estilo de presentación 1';
$string['sliderh4colordesc'] = 'Establece el color del texto de la etiqueta H4 cuando se utiliza el estilo de presentación 1.';

$string['slidersubmitcolor'] = 'Color del texto del botón del estilo de presentación 1';
$string['slidersubmitcolordesc'] = 'Establece el color del texto del botón cuando se utiliza el estilo de presentación 1.';

$string['slidersubmitbgcolor'] = 'Color del botón del estilo de presentación 1';
$string['slidersubmitbgcolordesc'] = 'Establece el color del botón cuando se utiliza el estilo de presentación 1.';

$string['slider2h3color'] = 'Color del texto H3 del estilo de presentación 2';
$string['slider2h3colordesc'] = 'Establece el color del texto de la etiqueta H3 cuando se utiliza el estilo de presentación 2.';

$string['slider2h4color'] = 'Color del texto H4 del estilo de presentación 2';
$string['slider2h4colordesc'] = 'Establece el color del texto de la etiqueta H4 cuando se utiliza el estilo de presentación 2.';

$string['slider2h3bgcolor'] = 'Color de fondo H3 del estilo de presentación 2';
$string['slider2h3bgcolordesc'] = 'Establece el color de fondo de la etiqueta H3 cuando se utiliza el estilo de presentación 2.';

$string['slider2h4bgcolor'] = 'Color de fondo H4 del estilo de presentación 2';
$string['slider2h4bgcolordesc'] = 'Establece el color de fondo de la etiqueta H4 cuando se utiliza el estilo de presentación 2.';

$string['slideroption2submitcolor'] = 'Color de fondo del enlace del estilo de presentación 2';
$string['slideroption2submitcolordesc'] = 'Establece el color de fondo del texto del enlace cuando se utiliza el estilo de presentación 2.';

$string['slideroption2color'] = 'Color de fondo de la presentación de diapositivas tipo 2';
$string['slideroption2colordesc'] = 'Establece el color de fondo de la diapositiva cuando se utiliza el estilo de presentación 2.';

$string['slideroption2a'] = 'Color de fondo de las flechas de la presentación de diapositivas tipo 2';
$string['slideroption2adesc'] = 'Establece el color de fondo de las flechas de la presentación de diapositivas cuando se utiliza el estilo de presentación 2.';

$string['sliderstyle1'] = 'Diapositivas estilo 1';
$string['sliderstyle2'] = 'Diapositivas estilo 2';


// Block Regions ***************************************************.
$string['blocklayoutbuilder'] = 'Constructor de regiones para bloques';
$string['blocklayoutbuilderdesc'] = 'A continuación puedes crear tu propia distribución de regiones para los bloques de moodle en la página principal.
Para añadir contenido a estas regiones necesitarás <strong>Activar edición en la página principal de Moodle</strong>.
Entonces podrás arrastrar y soltar los bloques en las regiones creadas.';

$string['dashblocklayoutbuilder'] = 'Regiones del Área Personal';
$string['dashblocklayoutbuilderdesc'] = 'A continuación puedes crear tu propia estructura de regiones para bloques en el Área Personal.
Para añadir dichas regiones necesitarás <strong>Activar Edición en la página del Área Personal</strong>.<br>
Entonces podrás arrastrar y soltar bloques en las regiones que has creado';

$string['coursepagesidebarinfooterenabledsection'] = 'Configuración común';
$string['coursepagesidebarinfooterenabledsectiondesc'] = 'Parámetros comunes para la mayoría de formatos de curso.';

$string['coursepageblocklayoutbuilder'] = 'Región para bloques en la página del curso';
$string['coursepageblocklayoutbuilderdesc'] = 'Puedes construir en la parte inferior una región para bloques en la página del curso.
Para añadir contenido a esas regiones necesitarás <strong>Activar Edición en la página del curso</strong>.
Entonces podrás comenzar a arrastrar y soltar los bloques en las regiones que has creado.';

$string['blocklayoutlayoutcheck'] = 'Comprueba la distribución';
$string['blocklayoutlayoutcheckdesc'] = 'Utiliza esta opción para ver la distribución creada y comprobar el número de bloques que has utilizado.';
$string['blocklayoutlayoutcount1'] = 'Puedes crear un máximo de ';
$string['blocklayoutlayoutcount2'] = ' regiones para bloques. Ahora estás usando: ';

$string['blocklayoutlayoutrow'] = 'Filas de regiones para bloques';
$string['blocklayoutlayoutrowdesc'] = 'Añade / establece la distribución de filas de las regiones para bloques de la página principal.';

$string['dashblocklayoutlayoutrow'] = 'Fila de regiones del Panel de Control ';
$string['dashblocklayoutlayoutrowdesc'] = 'Añade / establece la estructura de las regiones para bloques en la página del Área Personal.';

$string['coursepageblocklayouttoprow'] = 'Filas de la region superior de la página del curso ';
$string['coursepageblocklayouttoprowdesc'] = 'Añade o modifica filas en la estructura de la región superior para bloques de la página del curso.';

$string['coursepageblocklayouttop'] = 'Region superior';
$string['coursepageblocklayoutbottom'] = 'Region inferior';

$string['coursepageblocklayoutbottomrow'] = 'Filas de la region inferior de la página del curso ';
$string['coursepageblocklayoutbottomrowdesc'] = 'Añade o modifica filas en la estructura de la región inferior para bloques de la página del curso.';

$string['frontpageblocksenabled'] = 'Activa las regiones para bloques en la página principal';
$string['frontpageblocksenableddesc'] = 'Activa o desactiva las regiones para bloques de la página principal.
Puedes arrastrar y soltar los bloques en las regiones creadas.';

$string['dashblocksenabled'] = 'Activar las regiones personalizadas en la página del Área Personal';
$string['dashblocksenableddesc'] = 'Puedes activar / desactivar las regiones personalizadas de la página del Área Personal.
Puedes arrastrar y soltar los bloques en las regiones que has creado';

$string['dashblocksposition'] = 'Posición de la región de bloques personalizados';
$string['dashblockspositiondesc'] = 'Al activar la región de bloques personalizados para el Panel de Control, escoge la posición.';
$string['dashblocksabovecontent'] = 'Mostrar sobre el contenido';
$string['dashblocksbelowcontent'] = 'Mostrar bajo el contenido';

$string['coursepageblocksenabled'] = 'Mostrar las regiones personalizadas para bloques en la página del curso';
$string['coursepageblocksenableddesc'] = 'Puedes mostrar la región de bloques personalizados (superior e inferior) en la página del curso.
Podrás arrastrar y soltar los bloques en las regiones que has creado.';

$string['coursepagenewssliderblockregionheading'] = 'Región para bloque de diapositivas de noticias configurables';
$string['coursepagenewssliderblockregionheadingdesc'] = 'Una región creada especialmente para el bloque de noticias que es parte del conjunto de bloques Adaptable UI. Esta región se muestra sobre las acticidades del curso en la página de curso. Añade primero el bloque Noticias <strong><a href="https://moodle.org/plugins/block_news_slider">Bloque de Noticias para Adaptable</a></strong> y configuralo para mostrarlo en esta región';

$string['coursepageactivitybottomblockregionheading'] = 'Región inferior para bloques de actividades del curso';
$string['coursepageactivitybottomblockregionheadingdesc'] = 'Región para bloques que aparece en la parte inferior de la página de actividades.';

$string['coursepageblockactivitybottomenabled'] = 'Activar la región inferior para bloques de la página del curso';
$string['coursepageblockactivitybottomenableddesc'] = 'Activar la región para bloques situada en la parte inferior de la página del curso.';

$string['coursepagesidebarinfooterenabled'] = 'Mover la barra lateral al pie';
$string['coursepagesidebarinfooterenableddesc'] = 'Mover la barra lateral al pie de página para obtener más ancho de pantalla para el curso.';

$string['layoutcheck'] = 'Comprueba la estructura';
$string['layoutcheckdesc'] = 'Utiliza esta opción para ver la distribución creada y comprobar el número de bloques que has utilizado.';
$string['layoutcount1'] = 'Puedes crear un máximo de ';
$string['layoutcount2'] = ' regiones para bloques. Ahora estás usando: ';

$string['sidebaricon'] = 'Muestra / oculta la barra de navegación lateral';


// Marketing Blocks *************************************.
$string['marketingsettings'] = 'Bloques promocionales';
$string['marketingsettingsheading'] = 'Personaliza los bloques promocionales que aparecen en la página de inicio. Ver la distribución <a href="./../theme/adaptable/pix/layout.png" target="_blank">aqui</a>';
$string['marketingdesc'] = 'Dispones de dos cuadros informativos a los que puedes aplicar diferentes estilos. Además puedes activar el constructor de regiones
para bloques y decidir cuantos bloques quieres mostrar y su distribución en la página de inicio. Puedes ver la distribución de las diferentes regiones en el archivo  <a href="/adaptable/README.txt" target="_blank">README.txt</a>';

$string['marketingbuilderheading'] = 'Constructor de regiones para bloques promocionales';
$string['marketingbuilderdesc'] = 'Utiliza esta opción para ver la distribución creada y comprobar el número de bloques promocionales que has utilizado.';

$string['marketlayoutrow'] = 'Filas de regiones para bloques promocionales';
$string['marketlayoutrowdesc'] = 'Añade / establece la distribución de filas de las regiones para bloques promocionales de la página principal.';

$string['market'] = 'Bloque promocional ';
$string['marketdesc'] = 'Añade el código HTML del bloque promocional (ver el archivo <a href="./../theme/adaptable/README.txt" target="_blank">README</a> para más ejemplos).';

$string['layoutaddcontent'] = 'Añade contenido a los bloques promocionales';
$string['layoutaddcontentdesc1'] = 'Has configurado ';
$string['layoutaddcontentdesc2'] = ' bloques promocionales. Si estás conforme con la distribución añade el contenido de los bloques promocionales.
Si no estás conforme, usa el constructor de regiones para bloques promocionales para realizar cambios<br />';

$string['frontpagemarketenabled'] = 'Activar los bloques promocionales';
$string['frontpagemarketenableddesc'] = 'Activar los bloques promocionales en la pagina de inicio.';


// Footer **********************************************************.
$string['footersettings'] = 'Pie de Página';
$string['footersettingsheading'] = 'Configuracion básica';

$string['footerdesc'] = 'Controla el contenido que se muestra en las secciones del pie de página.';

$string['showfooterblocks'] = 'Mostrar bloques en el pie de página';
$string['showfooterblocksdesc'] = 'Mostrar los bloques personalizados en el pie de página.';

$string['footerblocksplacement'] = 'Páginas donde mostrar los bloques del pie de página';
$string['footerblocksplacementdesc'] = 'Definir los lugares donde se deben mostrar los bloques del pie de página. Todo el sitio es la opción por defecto.';
$string['footerblocksplacement1'] = 'Todo el sitio';
$string['footerblocksplacement2'] = 'Página Inicial';
$string['footerblocksplacement3'] = 'Nunca';

$string['footerlayoutrow'] = 'Constructor de regiones para los bloques del pie de página';
$string['footerlayoutrowdesc'] = 'Utiliza esta opción para ver la distribución creada y comprobar el número de bloques del pie de página que has utilizado.';

$string['footnote'] = 'Texto del bloque del pie de página ';
$string['footnotedesc'] = 'Añade el código HTML del bloque de pie de página (ver el archivo <a href="./../theme/adaptable/README.txt" target="_blank">README</a> para más ejemplos)';

$string['footerheader'] = 'Titulo del bloque del pie de página ';
$string['footerdesc'] = 'Añade el titulo del bloque del pie de página ';

$string['footercontent'] = 'Contenido del bloque del pie de página ';
$string['footercontentdesc'] = 'Añade el código HTML del bloque del pie de página (ver el archivo <a href="./../theme/adaptable/README.txt" target="_blank">README</a> para más ejemplos)';

$string['hidefootersocial'] = 'Mostrar iconos sociales';
$string['hidefootersocialdesc'] = 'Muestra los iconos sociales en el pie de página bajo los bloques.';

// Data Retention Summary Button.
$string['gdprbutton'] = 'Botón de Resumen de conservación de datos';
$string['gdprbuttondesc'] = 'Muestra el botón de Resumen de conservación de datos en el pie.';

// Moodle Docs link.
$string['moodledocs'] = 'Mostrar enlace a Moodle Docs';
$string['moodledocsdesc'] = 'Mostrar un enlace a Moodle Docs en el pie de página.';


// NavBar **********************************************************.
$string['stickynavbar'] = 'Fijar la barra de navegación';
$string['stickynavbardesc'] = 'Fijar la barra de navegación a la parte superior de la página.';

$string['navbarcachetime'] = 'Caché de la barra de navegación';
$string['navbarcachetimedesc'] = 'El tiempo, en minutos, que se guarda la barra de navegación.';

$string['navbarmenusettings'] = 'Menú Navegación';
$string['navbarmenusettingsheading'] = 'Personalizar el menú de la barra de navegación';
$string['navbarmenusettingsdesc'] = 'Añadir menús en la barra de navegación.';

$string['navbarsettings'] = 'Barra de navegación';
$string['navbarsettingsheading'] = 'Personalizar la barra de navegación';
$string['navbardesc'] = 'Controlar los diferentes elementos a mostrar en la barra de navegación.';

$string['navbarstyles'] = 'Estilos de la barra de navegación';
$string['navbarstylesheading'] = 'Personalixar los estilos de la barra de navegación';
$string['navbarstylesdesc'] = 'Permite personalizar los estilos de los elementos que aparecen en la barra de navegación.';

$string['navbarlinkssettings'] = 'Enlaces de la barra de navegación';
$string['navbarlinksettingsheading'] = 'Personaliza los enlaces de la barra de navegación';
$string['navbarlinksettingsdesc'] = 'Permite personalizar los enlaces que aparecen en la barra de navegación.';

$string['navbardisplayicons'] = 'Mostrar iconos';
$string['navbardisplayiconsdesc'] = 'Mostrar los iconos en los menus.';

$string['navbardisplaysubmenuarrow'] = 'Mostrar flecha de despliegue sub-menu';
$string['navbardisplaysubmenuarrowdesc'] = 'Mostrar la flecha (hacia abajo) de despliegue de sub-menu cuando los menus tengan sub-menus.';

$string['home'] = 'Inicio';
$string['enablemy'] = 'Área Personal';
$string['enablemydesc'] = 'Mostrar el enlace al Área Personal.';

$string['enableprofile'] = 'Perfil usuario';
$string['enableprofiledesc'] = 'Mostrar el enlace del perfil de usuario.';

$string['enableeditprofile'] = 'Editar perfil';
$string['enableeditprofiledesc'] = 'Mostrar el enlace para editar el perfil del usuario.';

$string['enablebadges'] = 'Insignias';
$string['enablebadgesdesc'] = 'Mostrar el enlace de las insignias del usuario.';

$string['enablegrades'] = 'Calificaciones';
$string['enablegradesdesc'] = 'Mostrar el enlace de las calificaciones del usuario.';

$string['enablecalendar'] = 'Calendario';
$string['enablecalendardesc'] = 'Mostrar el enlace al calendario del usuario.';

$string['enableprivatefiles'] = 'Archivos privados';
$string['enableprivatefilesdesc'] = 'Mostrar el enlace a los archivos privados del usuario.';

$string['enablesearchbox'] = 'Activar cuadro de búsqueda';
$string['enablesearchboxdesc'] = 'Mostrar el cuadro de búsqueda en la parte superior de la pantalla.';

$string['searchcourses'] = 'Search Courses';

$string['enablepref'] = 'Preferencias';
$string['enableprefdesc'] = 'Mostrar el enlace a las preferencias del usuario.';

$string['enablenote'] = 'Notificaciones';
$string['enablenotedesc'] = 'Mostrar el enlace página de notificaciones.';

$string['enableblog'] = 'Blogs del usuario';
$string['enableblogdesc'] = 'Mostrar el enlace a los blogs del usuario.';

$string['enableposts'] = 'Mis entradas';
$string['enablepostsdesc'] = 'Mostrar el enlace a las entradas en los foros realizadas por el usuario.';

$string['enablefeed'] = 'Mis opiniones';
$string['enablefeeddesc'] = 'Mostrar el enlace a la página "Opiniones" - <i>Nota: esta opción requiere el <a href="https://moodle.org/plugins/report_myfeedback" target="blank">plugin My Feedback</a> instalado';

$string['myblogs'] = 'Mis Blogs';

$string['noenrolments'] = 'No hay matriculaciones.';

$string['enablemyhomedesc'] = 'Mostrar un enlace a {$a}';
$string['enableeventsdesc'] = 'Mostrar un enlace al calendario';

$string['enablethiscoursedesc'] = 'Mostrar las actividades del curso actual';
$string['enablecoursesectionsdesc'] = 'Muestra el sub-menú  \'Este curso\' que contiene enlaces a las secciones.';
$string['sections'] = 'Secciones';

$string['enablecompetencieslink'] = 'Competencias';
$string['enablecompetencieslinkdesc'] = 'Mostrar el enlace a las competencias en el menu \'Este curso\' menu.  Nota: \'core_competency|enabled\' debe estar permitido.';

$string['enablecontentbank'] = 'Banco de Contenido';
$string['enablecontentbankdesc'] = 'Mostrar el enlace al Banco de Contenido.';


// Navbar styling *********************************************************.
$string['navbardropdownborderradius'] = 'Radio del borde del menú desplegable';
$string['navbardropdownborderradiusdesc'] = 'Modifica el radio del borde de los menus desplegables (esquinas redondeadas)';
$string['headernavbarstylingheading'] = 'Estilos Navbar';
$string['headernavbarstylingheadingdesc'] = 'Estilos para modificar los menus situados en la barra de navegación.';
$string['navbardropdownhovercolor'] = 'Color de fondo del menú desplegable al pasar el cursor';
$string['navbardropdownhovercolordesc'] = 'Establece el color de fondo del menú desplegable cuando pasa el cursor por encima.';
$string['navbardropdowntextcolor'] = 'Color del texto del menú desplegable';
$string['navbardropdowntextcolordesc'] = 'Establece el color del texto en los menús desplegables.';
$string['navbardropdowntexthovercolor'] = 'Color del texto del menu desplegable al pasar el cursor';
$string['navbardropdowntexthovercolordesc'] = 'Establece el color del texto de los items del menu desplegable al pasar el cursor por encima.';
$string['navbardropdowntransitiontime'] = 'Tiempo de transición';
$string['navbardropdowntransitiontimedesc'] = 'Tiempo del evento de transición en segundos.  Muestra un efecto de fundido cuando el cursor pasa sobre el menú que contiene sub-menús.';

// This Course menu *********************************************************.
$string['enablemysitesdesc'] = 'Muestra un desplegable con las actividades del curso y otras opciones';
$string['headernavbarthiscourseheading'] = 'Menú "Este curso"';
$string['headernavbarthiscourseheadingdesc'] = 'En este menú, el alumno puede acceder directamente todas las actividades del curso y a la lista de participantes y sus calificaciones.';

$string['displayparticipants'] = 'Participantes';
$string['displayparticipantsdesc'] = 'Muestra el item Participantes en el menú';
$string['displaygrades'] = 'Calificaciones';
$string['displaygradesdesc'] = 'Muestra el item Calificaciones en el menú';

// My courses menu *********************************************************.
$string['enablemysitesdesc'] = 'Mostrar los cursos del usuario';
$string['headernavbarmycoursesheading'] = 'Mis Cursos';
$string['headernavbarmycoursesheadingdesc'] = 'Todas las opciones del menú Mis Cursos (Mis Sitios) que muestran la lista de los cursos del usuario';

$string['enablemysitesrestriction'] = 'Restringir el acceso a &#34;Mis Cursos&#34;';
$string['enablemysitesrestrictiondesc'] = 'Restringir el acceso al menú "Mis Cursos" usando un campo de perfil personalizado. Ejemplo: usertype=alumno';

$string['mysitessortoverride'] = 'Activar el orden personalizado de "Mis Cursos"';
$string['mysitessortoverridedesc'] = 'Usa campos personalizados o textos (año, edad, ...) para colapsar los cursos en un desplegable.';
$string['mysitessortoverridefield'] = 'Campo personalizado o texto para filtrar "Mis Cursos"';
$string['mysitessortoverridefielddesc'] = 'Lista con delimitación por comas de los campos o textos a comprobar en el nombre corto de curso.';

$string['mysitessortoverrideoff'] = 'Mostrar lista de cursos.';
$string['mysitessortoverridestrings'] = 'Muestra todos los cursos en los que está matriculado el alumno en una lista. El resto son mostrados en un submenú.';
$string['mysitessortoverrideprofilefields'] = 'Muestra los cursos coincidentes con un campo personalizado en una lista. El resto son mostrados en un submenú.';
$string['mysitessortoverrideprofilefieldscohort'] = 'Muestra los cursos coincidentes con un campo de perfil y los cohortes en primer lugar. El resto son mostrados en un submenú.';

$string['mysitesmaxlength'] = 'Longitud máxima del título del curso en "Mis Cursos"';
$string['mysitesmaxlengthdesc'] = 'Ajusta la longitud máxima del título del curso a mostrar en "Mis Cursos". La longitud dependerá del tamaño y de la fuente usada.';

$string['mycoursesmenulimit'] = 'Límite menú Mis Cursos';
$string['mycoursesmenulimitdesc'] = 'Establece el número máximo de cursos que aparecen en el menú Mis Cursos. 0 mostrará todos los cursos.';

$string['mysitesmenudisplay'] = 'Modo visualización menú Mis Cursos';
$string['mysitesmenudisplaydesc'] = 'Elige que texto se ha de mostrar en los items del menú al pasar el cursos por encima.';
$string['mysitesmenudisplayshortcodenohover'] = 'Mostrar el código corto de curso y sin texto al pasar el cursor';
$string['mysitesmenudisplayshortcodefullnameonhover'] = 'Mostrar el código corto y el título del curso al pasar el cursor';
$string['mysitesmenudisplayfullnamenohover'] = 'Mostrar el título del curso sin texto al pasar el cursor';
$string['mysitesmenudisplayfullnamefullnameonhover'] = 'Mostrar el título largo del curso en el menú y al pasar el cursor';

$string['enablehomedesc'] = 'Mostrar enlace a Inicio';

$string['enablehomeredirect'] = 'Activar redirect=0';
$string['enablehomeredirectdesc'] = 'Activa la opción redirect=0 para redirigir a la página inicial del sitio. Está opción puede utilizarse en sitios que tienen el Área Personal como página inicial por defecto. Desactivada previene redirigir a los usuarios a la página inicial y ser redirigidos al Área Personal';


// Colours *********************************************************.
$string['colorsettings'] = 'Colores';
$string['colorsettingsheading'] = 'Modifica los principales colores utilizados en la plantilla.';
$string['colordesc'] = 'Puedes seleccionar los colores que desees para ser usados por la plantilla. Usa la notación HEX estándar. También puedes usar como valores <i>transparent</i> e <i>inherited.</i>';
$string['linkcolor'] = 'Color enlace';
$string['linkcolordesc'] = 'Establece el color de los enlaces.';

$string['linkhover'] = 'Color sobre enlace';
$string['linkhoverdesc'] = 'Establece el color cuando pasa el cursor sobre los enlaces.';

$string['backcolor'] = 'Color de fondo';
$string['backcolordesc'] = 'Establece el color de fondo de la plantilla.';

$string['regionmaincolor'] = 'Color de la región principal';
$string['regionmaincolordesc'] = 'Establece el color de fondo de la región principal.';

$string['maincolor'] = 'Color principal';
$string['maincolordesc'] = 'Establece el color principal del sitio.';

$string['footertextcolor'] = 'Color de texto del pie de página superior';
$string['footertextcolordesc'] = 'Establece el color de texto en el pie de página superior.';

$string['footerbkcolor'] = 'Color de fondo del pie de página superior';
$string['footerbkcolordesc'] = 'Establece el color de fondo del pie de página superior.';

$string['footertextcolor2'] = 'Color del texto del pie de página inferior';
$string['footertextcolor2desc'] = 'Establece el color del texto del pie de página inferior.';

$string['footerlinkcolor'] = 'Color de los enlaces del pie de página';
$string['footerlinkcolordesc'] = 'Establece el color de los enlaces del pie de página.';

$string['headerbkcolor'] = 'Color de fondo de la parte superior de la cabecera';
$string['headerbkcolordesc'] = 'Establece el color de fondo de la parte superior de la cabecera.';

$string['msgbadgecolor'] = 'Color de fondo del marcador de mensajes.';
$string['msgbadgecolordesc'] = 'Establece el color de fondo del marcador de mensajes en la cabecera (donde se muestra el número de mensajes no leidos)';

$string['msgbadgecolortext'] = 'Color del texto del marcador de mensajes';
$string['msgbadgecolortextdesc'] = 'Establece el color del texto del marcador de mensajes.';

$string['headerbkcolor2'] = 'Color de fondo de la parte inferior de la cabecera';
$string['headerbkcolor2desc'] = 'Establece el color de fondo de la parte inferior de la cabecera.';

$string['headertextcolor'] = 'Color de texto y enlaces de la parte superior de la cabecera';
$string['headertextcolordesc'] = 'Establece el color de texto y enlaces de la parte superior de la cabecera.';

$string['headertextcolor2'] = 'Color del texto y enlaces de la parte inferior de la cabecera';
$string['headertextcolor2desc'] = 'Establece el color de texto y enlaces de la parte inferior de la cabecera.';

$string['blockheadercolor'] = 'Color del texto de la cabecera de los bloques';
$string['blockheadercolordesc'] = 'Establece el color del texto de la cabecera de los bloques.';

$string['blockbackgroundcolor'] = 'Color de fondo de los bloques';
$string['blockbackgroundcolordesc'] = 'Establece el color de fondo de los bloques.';

$string['blockheaderbackgroundcolor'] = 'Color de fondo de los encabezados de los bloques';
$string['blockheaderbackgroundcolordesc'] = 'Establece el color de fondo de los encabezados de los bloques.';

$string['blockbordercolor'] = 'Color del borde de los bloques';
$string['blockbordercolordesc'] = 'Establece el color del borde de los bloques.';

$string['blockregionbackground'] = 'Color de fondo de las regiones para bloques';
$string['blockregionbackgrounddesc'] = 'Establece el color de fondo del contenedor del constructor de bloques de la página principal.';

$string['blockheaderbordertop'] = 'Grueso del borde superior de los bloques';
$string['blockheaderbordertopdesc'] = 'Establece el grosor del borde superior de los bloques.';

$string['blockheaderborderleft'] = 'Grueso del borde izquierdo de los bloques';
$string['blockheaderborderleftdesc'] = 'Establece el grosor del borde izquierdo de los bloques.';

$string['blockheaderborderright'] = 'Grueso del borde derecho de los bloques';
$string['blockheaderborderrightdesc'] = 'Establece el grosor del borde derecho de los bloques.';

$string['blockheaderborderbottom'] = 'Grueso del borde inferior de los bloques';
$string['blockheaderborderbottomdesc'] = 'Establece el grosor del borde inferior de los bloques.';

$string['blockmainbordertop'] = 'Grueso del borde superior del bloque central';
$string['blockmainbordertopdesc'] = 'Establece el grosor del borde superior del bloque central.';

$string['blockmainborderleft'] = 'Grueso del borde izquierdo del bloque central';
$string['blockmainborderleftdesc'] = 'Establece el grosor del borde izquierdo del bloque central.';

$string['blockmainborderright'] = 'Grueso del borde derecho del bloque central';
$string['blockmainborderrightdesc'] = 'Establece el grosor del borde derecho del bloque central.';

$string['blockmainborderbottom'] = 'Grueso del borde inferior del bloque central';
$string['blockmainborderbottomdesc'] = 'Establece el grosor del borde inferior del bloque central.';

$string['blockheaderbordertopstyle'] = 'Estilo del borde de los bloques';
$string['blockheaderbordertopstyledesc'] = 'Establece el estilo del borde de los bloques laterales.';

$string['blockmainbordertopstyle'] = 'Estilo del borde del bloque central';
$string['blockmainbordertopstyledesc'] = 'Establece el estilo del borde de los bloques centrales.';

$string['blockheadertopradius'] = 'Radio de las esquinas superiores de los bloques laterales';
$string['blockheadertopradiusdesc'] = 'Radio de las esquinas superiores de los bloques laterales. Valor más alto es igual a efecto redondeado.';

$string['blockheaderbottomradius'] = 'Radio de las esquinas inferiores de los bloques laterales';
$string['blockheaderbottomradiusdesc'] = 'Radio de las esquinas inferiores de los bloques laterales. Valor más alto es igual a efecto redondeado.';

$string['blockmaintopradius'] = 'Radio de las esquinas superiores de los bloques centrales';
$string['blockmaintopradiusdesc'] = 'Radio de las esquinas superiores de los bloques centrales. Valor más alto es igual a efecto redondeado.';

$string['blockmainbottomradius'] = 'Radio de las esquinas inferiores de los bloques centrales';
$string['blockmainbottomradiusdesc'] = 'Radio de las esquinas inferiores de los bloques centrales. Valor más alto es igual a efecto redondeado.';

$string['marketblockbordercolor'] = 'Color del borde de los bloques promocionales';
$string['marketblockbordercolordesc'] = 'Establece el color del borde de los bloques promocionales.';

$string['marketblocksbackgroundcolor'] = 'Color de fondo de la región de los bloques promocionales.';
$string['marketblocksbackgroundcolordesc'] = 'Establece el color de fondo de la región de los bloques promocionales.';

$string['blocklinkcolor'] = 'Color del enlace de los menus en los bloques';
$string['blocklinkcolordesc'] = 'Establece el color del enlace de los menus situados en los bloques.';

$string['blocklinkhovercolor'] = 'Color del enlace de los menus en los bloques al pasar por encima';
$string['blocklinkhovercolordesc'] = 'Establece el color del enlace de los menus en los bloques al pasar por envcima el cursor.';

$string['sectionheadingcolor'] = 'Color del texto de la cabecera de sección';
$string['sectionheadingcolordesc'] = 'Establece el color del texto de la cabecera del tema actual.';

$string['homebk'] = 'Imagen de fondo';
$string['homebkdesc'] = 'Carga la imagen de fondo para la plantilla.';

$string['dividingline'] = 'Color de la línea divisoria de la cabecera';
$string['dividinglinedesc'] = 'Establece el color de la línea divisoria de la cabecera. Usar el mismo color que la cabecera para eliminarla.';

$string['dividingline2'] = 'Color de la línea divisoria del pie de página';
$string['dividingline2desc'] = 'Establece el color de la línea divisoria del pie de página. Usar el mismo color que el pie para eliminarla.';

$string['breadcrumbbackgroundcolor'] = 'Color de fondo de la guía de exploración';
$string['breadcrumbbackgroundcolordesc'] = 'Establece el color de fondo de la guía de exploración.';

$string['breadcrumbtextcolor'] = 'Color de texto de la guía de exploración';
$string['breadcrumbtextcolordesc'] = 'Establece el color de texto de la guía de exploración.';

$string['activebreadcrumb'] = 'Color de fondo de la guía de exploración activa';
$string['activebreadcrumbdesc'] = 'Establece el color de fondo de la guía de exploración activa.';

$string['menubkcolor'] = 'Color de fondo del menú principal';
$string['menubkcolordesc'] = 'Establece el color de fondo del menú principal.';

$string['menubordercolor'] = 'Color de la línea divisoria de la barra de navegación';
$string['menubordercolordesc'] = 'Establece el color de la linea divisoria de la barra de navegación.';

$string['menufontcolor'] = 'Color de texto del menú principal';
$string['menufontcolordesc'] = 'Establece el color del texto del menú principal.';

$string['menuhovercolor'] = 'Color de fondo sobre el menú principal';
$string['menuhovercolordesc'] = 'Establece el color de fondo del menú principal cuando pasa el cursor sobre los enlaces.';

$string['mobilemenubkcolor'] = 'Color de fondo del menú para móvil';
$string['mobilemenubkcolordesc'] = 'Establece el color de fondo del menú principal en móviles (colapsado)';

$string['mobileslidebartabbkcolor'] = 'Color de fondo del panel lateral';
$string['mobileslidebartabbkcolordesc'] = 'Establece el color de fondo del panel lateral en dispositivos móviles (colapsado)';

$string['mobileslidebartabiconcolor'] = 'Color del icono del panel lateral';
$string['mobileslidebartabiconcolordesc'] = 'Establece el color del icono del panel lateral en dispositivos móviles (colapsado)';

$string['mobilemenufontcolor'] = 'Color del texto del menú para móvil';
$string['mobilemenufontcolordesc'] = 'Establece el color de texto del menú principal en móviles (colapsado)';

$string['selectiontext'] = 'Color del texto seleccionado';
$string['selectiontextdesc'] = 'Establece el color del texto seleccionado en pantalla.';

$string['selectionbackground'] = 'Color de fondo del texto seleccionado';
$string['selectionbackgrounddesc'] = 'Establece el color de fondo del texto seleccionado en pantalla.';

$string['formsbackgroundcolor'] = 'Forms background colour';
$string['formsbackgroundcolordesc'] = 'Set the background colour for forms elements: text area, text box and selects.';

$string['formstextcolor'] = 'Forms text colour';
$string['formstextcolordesc'] = 'Set the text colour for forms elements: text area, text box and selects.';

$string['settingsheaderbgcolor'] = 'Settings page heading background colour';
$string['settingsheaderbgcolordesc'] = 'Set the background colour for the heading used to separate the sections in the settings pages.';

$string['settingsheadertextcolor'] = 'Settings page heading text colour';
$string['settingsheadertextcolordesc'] = 'Set the text colour for the heading used to separate the sections in the settings pages.';


// Course Formats *********************************************************.
$string['coursesettings'] = 'Formatos de curso';
$string['coursesettingsheading'] = 'Configuración de los formatos de curso.';
$string['coursesettingsdesc'] = 'Configura algunas de las opciones de los formatos de curso más utilizados en moodle para ajustarlos al diseño general. Actualmente se pueden personalizar el formato poe defecto Temas/Semanas y también el Muro Social';

// Common settings.
$string['commonlyusedar'] = 'Actividades y recursos más utilizados';
$string['commonlyusedardesc'] = 'Introduce aquí la lista de actividades/recursos (separadas por comas) que deseas ver al abrir la ventana de selección. Algunos de los nombres posibles son {$a}.';
$string['commonlyusedartitle'] = 'Recursos y actividades más usadas';

$string['showyourprogress'] = 'Mostrar la etiqueta &#39;Su progreso&#39; ';
$string['showyourprogressdesc'] = 'Muestra / oculta la etiqueta &#39;Su progreso&#39; en la parte superior del contenido del curso. Esta etiqueta es sólo informativa y puede ocultarse al alumno.';

$string['currentcolor'] = 'Color de la sección actual';
$string['currentcolordesc'] = 'Establece el color de la sección actual dentro del curso.';

// Course Section background color.
$string['coursesectionbgcolor'] = 'Color de fondo de la sección del curso';
$string['coursesectionbgcolordesc'] = 'Establece el color de fondo de la sección del curso.';

// Topics / Weeks Settings.
$string['topicsweeks'] = 'Formato de curso Temas/Semanas';
$string['topicsweeksdesc'] = 'Establece estilos para el formato de curso Temas/semanas.';

$string['coursesectionheaderbg'] = 'Color de fondo de la cabecera';
$string['coursesectionheaderbgdesc'] = 'Establece el color de fondo de la cabecera de la sección del curso.';

$string['currentcolor'] = 'Color de realce de la sección actual';
$string['currentcolordesc'] = 'Establece el color de realce de la sección actual.';

$string['coursesectionheaderborderstyle'] = 'Estilo del borde inferior de la cabecera';
$string['coursesectionheaderborderstyledesc'] = 'Establece el estilo del borde inferior de la cabecera de la sección del curso.';

$string['coursesectionheaderbordercolor'] = 'Color del borde inferior de la cabecera';
$string['coursesectionheaderbordercolordesc'] = 'Establece el color del borde inferior de la cabecera de la sección del curso.';

$string['coursesectionheaderborderwidth'] = 'Grueso del borde inferior de la cabecera';
$string['coursesectionheaderborderwidthdesc'] = 'Establece el grueso del borde inferior de la cabecera de la sección del curso.';

$string['coursesectionheaderborderradiustop'] = 'Radio de las esquinas del borde superior de la cabecera';
$string['coursesectionheaderborderradiustopdesc'] = 'Establece el radio de las esquinas del borde superior de la cabecera de la sección del curso.';

$string['coursesectionheaderborderradiusbottom'] = 'Radio de las esquinas del borde inferior de la cabecera';
$string['coursesectionheaderborderradiusbottomdesc'] = 'Establece el radio de las esquinas del borde inferior de la cabecera de la sección del curso.';

$string['coursesectionborderstyle'] = 'Estilo del borde de la sección del curso';
$string['coursesectionborderstyledesc'] = 'Establece el estilo del borde de la sección del curso.';

$string['coursesectionborderwidth'] = 'Grueso del borde de la sección';
$string['coursesectionborderwidthdesc'] = 'Establece el grueso del borde de la sección del curso.';

$string['coursesectionbordercolor'] = 'Color del borde de la sección';
$string['coursesectionbordercolordesc'] = 'Establece el color del borde de la sección del curso.';

$string['coursesectionborderradius'] = 'Radio del borde de la sección';
$string['coursesectionborderradiusdesc'] = 'Establece el radio del borde de las esquinas de la sección del curso.';

// Estilos de actividades.
$string['coursesectionactivityuseadaptableicons'] = 'Usar el juego de iconos de Adaptable';
$string['coursesectionactivityuseadaptableiconsdesc'] = 'Usar el juego de iconos de actividades de Adaptable en lugar del de Moodle. Si lo desactivas, comprueba que los directorios <i>theme/adaptable/pix_plugins</i> y <i>theme/adaptable/pix_core/f</i> están eliminados para poder usar los icomos por defecto de moodle.';

$string['coursesectionactivityiconsize'] = 'Tamaño de los iconos de actividades de las secciones del curso';
$string['coursesectionactivityiconsizedesc'] = 'Establece el tamaño de los iconos de actividades / recursos (ejemplo: 16px establece un tamaño de 16px x 16px).';

$string['coursesectionactivityheadingcolour'] = 'Color de la cabecera de sección del curso';
$string['coursesectionactivityheadingcolourdesc'] = 'Establece el color de las actividades mostradas en la página del curso.';

// These four settings actually refer to bottom border (it was originally all around border, but naming kept as it was originally).
$string['coursesectionactivityborderwidth'] = 'Grueso del borde de las secciones de los cursos';
$string['coursesectionactivityborderwidthdesc'] = 'Establece el grueso de borde de las secciones de actividades del curso.';
$string['coursesectionactivityborderstyle'] = 'Course Section Activity Bottom Border Style';
$string['coursesectionactivityborderstyledesc'] = 'Set the style of the course section activity bottom border.';
$string['coursesectionactivitybordercolor'] = 'Course Section Activity Bottom Border Colour ';
$string['coursesectionactivitybordercolordesc'] = 'Set the colour of the course section activity bottom border.';
$string['coursesectionactivityleftborderwidth'] = 'Course Section Activity Left Border Width';
$string['coursesectionactivityleftborderwidthdesc'] = 'Set width of the border that appears on the left of a course section activity.';

$string['coursesectionactivitycolors'] = 'Opciones de las secciones del curso';

$string['coursesectionactivityassignleftbordercolor'] = 'Color del borde izquierdo de la actividad Tarea';
$string['coursesectionactivityassignleftbordercolordesc'] = 'Establece el color del borde izquierdo de la actividad Tarea.';
$string['coursesectionactivityassignbgcolor'] = 'Color de fondo de la actividad Tarea';
$string['coursesectionactivityassignbgcolordesc'] = 'Establece el color de fondo de la actividad Tarea.';

$string['coursesectionactivityforumtopbordercolor'] = 'Color del borde superior de la actividad Foro';
$string['coursesectionactivityforumtopbordercolordesc'] = 'Establece el color del borde superior de la actividad Foro.';
$string['coursesectionactivityforumbgcolor'] = 'Color de fondo de la actividad Foro';
$string['coursesectionactivityforumbgcolordesc'] = 'Establece el color de fondo de la actividad Foro.';

$string['coursesectionactivityquiztopbordercolor'] = 'Color del borde superior de la actividad Cuestionario';
$string['coursesectionactivityquiztopbordercolordesc'] = 'Establece el color del borde superior de la actividad Cuestionario.';
$string['coursesectionactivityquizbgcolor'] = 'Color de fondo de la actividad Cuestionario';
$string['coursesectionactivityquizbgcolordesc'] = 'Establece el color de fondo de la actividad Cuestionario.';

// Social Wall Settings.
$string['socialwall'] = 'Formato Muro Social';
$string['socialwallheading'] = 'Configuración Muro Social';
$string['socialwalldesc'] = 'Personaliza la apariencia del <a href="https://moodle.org/plugins/format_socialwall">formato de curso Muro Social</a> (si está instalada en el sitio)';

$string['socialwallbackgroundcolor'] = 'Color de fondo';
$string['socialwallbackgroundcolordesc'] = 'Establece el color de fondo del formato Muro Social.';

$string['socialwallsectionradius'] = 'Radio del borde';
$string['socialwallsectionradiusdesc'] = 'Establece el radio del borde de las secciones del Muro Social.';

$string['socialwallbordertopstyle'] = 'Estilo del borde';
$string['socialwallbordertopstyledesc'] = 'establece el estilo del borde de las secciones del Muro Social.';

$string['socialwallborderwidth'] = 'Ancho del borde';
$string['socialwallborderwidthdesc'] = 'Establece el ancho del borde de las secciones del Muro Social.';

$string['socialwallbordercolor'] = 'Color del borde';
$string['socialwallbordercolordesc'] = 'Color del borde de las secciones del Muro Social.';

$string['socialwallactionlinkcolor'] = 'Color de los enlaces';
$string['socialwallactionlinkcolordesc'] = 'Establece el color de los enlaces.';

$string['socialwallactionlinkhovercolor'] = 'Color hover de los enlaces';
$string['socialwallactionlinkhovercolordesc'] = 'Establece el color hover de los enlaces.';


// Fonts ***********************************************************.
$string['fontname'] = 'Fuente principal';
$string['fontnamedesc'] = 'Establece la fuente principal. Selecciona defecto o una fuente de <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a>.';

$string['fontsize'] = 'Tamaño de la fuente principal';
$string['fontsizedesc'] = 'Selecciona el tamaño por defecto de la fuente principal (en porcentaje) usada en el sitio.';

$string['fontweight'] = 'Grueso de la fuente principal';
$string['fontweightdesc'] = 'Establece el grueso de la fuente principal. Introducir un valor entre 100 y 900 dependiendo de la fuente utilizada.';

$string['fontcolor'] = 'Color de la fuente principal';
$string['fontcolordesc'] = 'Establece el color de la fuente principal.';

$string['fontheadername'] = 'Fuente encabezamientos';
$string['fontheadernamedesc'] = 'Establece la fuente de los encabezamientos utilizados en la plantilla. Selecciona default o una fuente de <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a>.';

$string['fontheadercolor'] = 'Color de la fuente de encabezamientos';
$string['fontheadercolordesc'] = 'Establece el color de la fuente de los encabezamientos utilizados en la plantilla.';

$string['fontheaderweight'] = 'Grueso de la fuente de encabezamientos';
$string['fontheaderweightdesc'] = 'Establece el grueso de la fuente de los encabezamientos. Introducir un valor entre 100 y 900 dependiendo de la fuente utilizada.';

$string['fonttitlename'] = 'Fuente del título del sitio';
$string['fonttitlenamedesc'] = 'Establece la fuente del título del sitio situado en la cabecera. Selecciona default o una fuente de <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a>.';

$string['fonttitlecolor'] = 'Color de la fuente del título del sitio';
$string['fonttitlecolordesc'] = 'Establece el color de la fuente del título del sitio situado en la cabecera.';

$string['fonttitleweight'] = 'Grueso de la fuente del título del sitio';
$string['fonttitleweightdesc'] = 'Establece el grueso de la fuente del título del sitio. Introducir un valor entre 100 y 900 dependiendo de la fuente utilizada.';

$string['fonttitlesize'] = 'Tamaño de la fuente del título del sitio';
$string['fonttitlesizedesc'] = 'Tamaño de la fuente del título del sitio situado en la cabecera. Introducir un valor decimal, por ejemplo: 24px';

$string['fonttitlecolorcourse'] = 'Color de la fuente del titulo del curso';
$string['fonttitlecolorcoursedesc'] = 'Establece el color de la fuente utilizada en el titulo del curso de la cabecera.';

$string['fontsubset'] = 'Juego de caracteres de Google Fonts';
$string['fontsubsetdesc'] = 'Seleccionar el juego de caracteres de Google Fonts a usar además del latín ya incluido por defecto. Se aplicará a toda las fuentes seleccionadas.';

$string['menufontsize'] = 'Tamaño de la fuente de la barra de navegación';
$string['menufontsizedesc'] = 'Establece el tamaño de la fuente utilizada en la barra de navegación.';

$string['menufontpadding'] = 'Separación entre los ítems de la barra de navegación';
$string['menufontpaddingdesc'] = 'Establece la separación entre los ítems de la barra de navegación.';

$string['fontblockheadercolor'] = 'Color de la fuente de la cabecera de los bloques';
$string['fontblockheadercolordesc'] = 'Establece el color de la fuente de la cabecera de los bloques de moodle.';

$string['fontblockheaderweight'] = 'Grueso de la fuente de la cabecera de los bloques de moodle';
$string['fontblockheaderweightdesc'] = 'Establece grueso de la fuente de la cabecera de los bloques de moodle. Seleccionar un valor entre 100 y 900 dependiendo de la fuente seleccionada.';

$string['fontblockheadersize'] = 'Tamaño de la fuente de la cabecera de la cabecera de los bloques de moodle';
$string['fontblockheadersizedesc'] = 'Establece el tamaño de la fuente utilizada en la cabecera de los bloques de moodle. Seleccionar un valor de la lista.';

$string['fonttopmenusize'] = 'Tamaño de fuente del menú superior';
$string['fonttopmenusizedesc'] = 'Establece el tamaño de la fuente del menú de la barra superior. Selecciona un valor de la lista (el valor por defecto es 1rem = 16px)';

$string['fontmenusize'] = 'Tamaño de la fuente de la barra de navegación';
$string['fontmenusizedesc'] = 'Establece el tamaño de la fuente de la barra de navegación. Selecciona un valor de la lista (el valor por defecto es 1rem = 16px)';

// Icons ***********************************************************.
$string['blockicons'] = 'Iconos de bloques';
$string['blockiconsdesc'] = 'Establece si se muestran los iconos en la cabecera de los bloques.';

$string['blockiconsheadersize'] = 'Tamaño iconos de bloques';
$string['blockiconsheadersizedesc'] = 'Establece el tamaño del icono mostrado en la cabecera de los bloques. Seleccionar un valor de la lista.';


// Buttons *********************************************************.
$string['buttonsettingsheading'] = 'Personalizar los botones.';
$string['buttondesc'] = 'Personaliza a forma y colores de los botones utilizados en la plantilla.';

$string['buttonradius'] = 'Radio de las esquinas';
$string['buttonradiusdesc'] = '0 = esquinas cuadradas. Un valor más alto es igual a esquinas más redondeadas.';

$string['buttoncolor'] = 'Colores de los botones';
$string['buttoncolordesc'] = 'El color de los botones principales.';

$string['buttonhovercolor'] = 'Color Hover de los botones';
$string['buttonhovercolordesc'] = 'Color del botón al pasar el cursor por encima.';

$string['buttontextcolor'] = 'Color del texto de los botones';
$string['buttontextcolordesc'] = 'Establece el color del texto de los botones.';

$string['buttondropshadow'] = 'Sombra de la parte superior del botón';
$string['buttondropshadowdesc'] = 'Muestra una ligera sombra (sombreado) en la parte superior del botón.';


// Edit button.
$string['editonbk'] = 'Color de fondo de los botones de edición y personalización activados';
$string['editonbkdesc'] = 'Establece el color de fondo de los botones de edición y personalización cuando están activados.';

$string['editoffbk'] = 'Color de fondo de los botones de edición y personalización desactivados';
$string['editoffbkdesc'] = 'Establece el color de fondo de los botones de edición y personalización cuando están desactivados.';

$string['editfont'] = 'Color del texto de los botones de <i>Edición</i> y <i>Personalizar esta página</i>';
$string['editfontdesc'] = 'Establece el color del texto de los botones de <i>Edición</i> y <i>Personalizar esta página</i>';

$string['editverticalpadding'] = 'Espaciado vertical de los botones de edición';
$string['edithorizontalpadding'] = 'Espaciado horizontal de los botones de edición';
$string['edittopmargin'] = 'Margen superior de los botones de edición';

// Cancel button.
$string['buttoncancelbackgroundcolor'] = 'Color de fondo para el botón Cancelar';
$string['buttoncancelbackgroundcolordesc'] = 'Establece el color de fondo para el botón Cancelar.  Introduce <i>transparent</i> para mostrar el fondo transparente.';

$string['buttoncancelcolor'] = 'Color del texto del botón Cancelar';
$string['buttoncancelcolordesc'] = 'Establece el color de texto para el botón Cancelar.';


// Login button.
$string['logintextbutton'] = 'Entrar';

$string['buttonlogincolor'] = 'Color del botón Entrar';
$string['buttonlogincolordesc'] = 'Color del botón Entrar.';

$string['buttonloginhovercolor'] = 'Color Hover del botón Entrar';
$string['buttonloginhovercolordesc'] = 'Color del botón Entrar al pasar el cursor por encima.';

$string['buttonlogintextcolor'] = 'Color del texto del botón Entrar';
$string['buttonlogintextcolordesc'] = 'Color del texto del botón Entrar.';

$string['loginplaceholder'] = 'Usuario';
$string['passwordplaceholder'] = 'Contraseña';


// Header ***********************************************************.
$string['headersettings'] = 'Cabecera';
$string['headersettingsheading'] = 'Personalización de la cabecera.';
$string['headerdesc'] = 'Mostrar Alertas para avisar a los usuarios, controlar la barra de navegación, mostrar el título del sitio o el logo son algunas de las opciones de la cabecera.';


$string['headerbgimage'] = 'Imagen de fondo';
$string['headerbgimagedesc'] = 'Establece una imagen de fondo para la cabecera. El tamaño mínimo es 1600x180px (1900x180px recomendado). La imagen ocupará toda la cabecera. Puedes añadir un color &#39;Color de fondo de la cabecera superior&#39; o usar <i>transparent</i> para mostrar la imagen. En ese caso, modificar el color del texto  para mostrarlo correctamente sobre la imagen.';

$string['enableheading'] = 'Mostrar nombre del curso';
$string['enableheadingdesc'] = 'Establece el modo de visualización del título del curso en la cabecera.';

$string['breadcrumbdisplay'] = 'Mostrar la guía de exploración';
$string['breadcrumbdisplaydesc'] = 'Establece que se muestra en el área de la guía de exploración del curso.';

$string['sitetitlecoursesdisabled'] = 'Desactivado - muestra solo el título del curso en las páginas del curso';
$string['sitetitlecoursesenabled'] = 'Activado - muestra el nombre del sitio y el título del cursos en las páginas del curso';

$string['sitetitlepaddingtop'] = 'Separación superior del nombre del sitio';
$string['sitetitlepaddingtopdesc'] = 'Permite ajustar la separación, en píxeles, de la parte superior del nombre del sitio.';

$string['sitetitlepaddingleft'] = 'Separación izquierda del nombre del sitio';
$string['sitetitlepaddingleftdesc'] = 'Permite ajustar la separación, en píxeles, de la parte izquierda del nombre del sitio.';

$string['sitetitlemaxwidth'] = 'Longitud máxima del nombre del sitio';
$string['sitetitlemaxwidthdesc'] = 'Establece la longitud máxima de caracteres del nombre del sitio. La longitud final dependerá de la fuente, tamaño y resolución de pantalla.';

$string['coursetitlepaddingtop'] = 'Separación superior del título del curso';
$string['coursetitlepaddingtopdesc'] = 'Permite ajustar la separación, en píxeles, de la parte superior del título del curso.';

$string['coursetitlepaddingleft'] = 'Separación izquierda del título del curso';
$string['coursetitlepaddingleftdesc'] = 'Permite ajustar la separación, en píxeles, de la parte izquierda del título del curso.';

$string['coursetitlemaxwidth'] = 'Ancho máximo del título del curso';
$string['coursetitlemaxwidthdesc'] = 'Establece el ancho máximo del área donde se muestra el título del curso.';

$string['coursepageheaderhidesitetitle'] = 'Ocultar el título del sitio en la página del curso';
$string['coursepageheaderhidesitetitledesc'] = 'Ocultar el título del sitio, logo y caja de búsqueda en la página del curso. Usar junto a la configuración de la altura de la cabecera de página para mostrar una cabecera más reducida en las páginas relacionadas con cursos.';

$string['breadcrumb'] = 'Ruta de navegación';
$string['breadcrumbtitle'] = 'Nombre del curso en la ruta de navegación';
$string['breadcrumbtitledesc'] = 'Establece el modo de visualización del título del curso en la ruta de navegación.';

$string['coursetitlefullname'] = 'Nombre completo del curso';
$string['coursetitleshortname'] = 'Nombre corto del curso / Código';

$string['headerstyle'] = 'Estilo de la cabecera';
$string['headerstyledesc'] = 'Selecciona el estilo de la cabecera. Cabecera 1 es la original de 3 filas de Adaptable. Cabecera 2 es la cabecera reducida a 2 filas. Ten en cuenta que usando la Cabecera 2, el parámetro <i>"Mostrar cuadro de búsqueda o iconos sociales"</i> siempre mostrará el cuadro de búsqueda.';
$string['headerstyle1'] = 'Cabecera 1 (3 filas)';
$string['headerstyle2'] = 'Cabecera 2 (2 filas)';

$string['header2searchbox'] = 'Cuadro de búsqueda expandible (Cabecera 2)';
$string['header2searchboxdesc'] = 'Expande y colapsa el cuadro de búsqueda al utilizar la Cabecera 2.';

$string['socialorsearch'] = 'Mostrar cuadro de búsqueda';
$string['socialorsearchdesc'] = 'Mostrar un cuadro de búsqueda en la cabecera.';

$string['socialorsearchnone'] = 'Ninguno';
$string['socialorsearchsearch'] = 'Mostrar cuadro de búsqueda';

$string['searchboxpadding'] = 'Separación alrededor del cuadro de búsqueda';
$string['searchboxpaddingdesc'] = 'Establece la separación de la parte superior del cuadro de búsqueda (si se muestra en lugar de los iconos sociales) <br />Ejemplo: 5px 10px 5px 10px (superior, derecha, inferior, izquierda).<br> Puedes establecer los iconos sociales en la sección <a href="./../admin/settings.php?section=theme_adaptable_social">Redes sociales</a></strong>.';

$string['enablesavecanceloverlay'] = 'Guardar / Cancelar sobrepuesto en las páginas de configuración';
$string['enablesavecanceloverlaydesc'] = 'Muestra los botones de Guardar / Cancelar sobrepuestos en la parte superior de las páginas de configuración para facilitar el guardado de los cambios.';

$string['usernavheading'] = 'Personaliza el desplegable de la barra de usuario';
$string['usernav'] = 'Barra de usuario';
$string['usernavdesc'] = 'Permite mostrar los elementos que aparecen en el desplegable de la barra de usuario.';

$string['showusername'] = 'Mostrar nombre del usuario';
$string['showusernamedesc'] = 'Muestra el nombre de usuario en la barra de navegación.';

$string['usernameposition'] = 'Posición del nombre de usuario';
$string['usernamepositiondesc'] = 'Establece la posición del nombre de usuario (Derecha o Izquierda).';

$string['menusettings'] = 'Menús de cabecera';
$string['menusettingsheading'] = 'Personaliza los menús de la cabecera superior.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Sube el archivo con la imagen del logo (170px x 80px max.). Formatos admitidos: .png, .jpg y .gif';

$string['favicon'] = 'Favicon';
$string['favicondesc'] = 'Carga el archivo favicon.ico que se muestra en la pestaña del navegador.';

$string['enableavailablecourses'] = 'Mostrar el texto "Cursos Disponibles"';
$string['enableavailablecoursesdesc'] = 'Mostrar el texto <i>Cursos Disponibles</i> sobre el listado de cursos en la página inicial.';

$string['thiscourse'] = 'Este curso';
$string['coursesections'] = 'Secciones del curso';

$string['loadtime'] = 'Página cargada en ';

$string['loadingcolor'] = 'Color del indicador de carga';
$string['loadingcolordesc'] = 'Color del indicador de carga situado en la parte superior de la página.';

$string['sitetitle'] = 'Mostrar título del sitio';
$string['sitetitledesc'] = 'Muestra el nombre del sitio indicado en <a href="./../admin/settings.php?section=frontpagesettings" target="_blank">Front Page Settings</a> o introduce el texto del nombre del sitio a continuación. Si añades una imagen para el logo, entonces el titulo no se mostrará.';

$string['sitetitleoff'] = 'No mostrar nombre del sitio';
$string['sitetitledefault'] = 'Mostrar nombre del sitio de Moodle (site name)';
$string['sitetitlecustom'] = 'Mostrar nombre del sitio personalizado (introducir el texto en el siguiente campo)';

$string['sitetitletext'] = 'Nombre del sitio';
$string['sitetitletextdesc'] = 'El nombre del sitio que se mostrará en la cabecera.';

$string['displaylogin'] = 'Mostrar la entrada de usuario';
$string['displaylogindesc'] = 'Selecciona el método de entrada del usuario (cuando esté activado)';

$string['displayloginbutton'] = 'Botón';
$string['displayloginbox'] = 'Formulario de entrada';
$string['displayloginno'] = 'Sin formulario de entrada';

$string['hideblocks'] = 'Ocultar bloques';
$string['showblocks'] = 'Mostrar bloques';
$string['fullscreen'] = 'Pantalla completa';
$string['standardview'] = 'Vista estándar';
$string['sitelinkslabel'] = 'Enlaces';

$string['viewselect'] = 'Pantalla completa como vista por defecto';
$string['viewselectdesc'] = 'Establecer la vista por defecto a pantalla completa (cuando la opción zoom está activada)';

$string['enablezoom'] = 'Activar Zoom';
$string['enablezoomdesc'] = 'Permite a los usuarios cambiar entre pantalla completa o ancho fijo de pantalla.';
$string['enablezoomshowtext'] = 'Mostrar el texto &#34;Activar Zoom&#34;';
$string['enablezoomshowtextdesc'] = 'Muestra el texto al lado del botón.';

$string['defaultzoom'] = 'Tamaño de pantalla';
$string['defaultzoomdesc'] = 'Tamaño de pantalla por defecto cuando el zoom está desactivado o el usuario no tiene preferencia. Selecciona entre Pantalla completa o Ancho fijo.';
$string['normal'] = 'Ancho fijo';
$string['wide'] = 'Pantalla completa';

$string['enableshowhideblocks'] = 'Activar Mostrar/Ocultar bloques';
$string['enableshowhideblocksdesc'] = 'Permite a los usuarios mostrar / ocultar los bloques laterales.';
$string['enableshowhideblockstext'] = 'Muestra el texto para &#34;Mostrar bloques&#34;';
$string['enableshowhideblockstextdesc'] = 'Muestra el texto al lado del botón.';

$string['enablenavbarwhenloggedout'] = 'Mostrar la barra de navegación para visitantes';
$string['enablenavbarwhenloggedoutdesc'] = 'Muestra la barra de navegación cuando estás fuera del sitio. Limitado al menú de Ayuda y Herramientas.';

$string['fullscreenwidth'] = 'Ancho de pantalla completa';
$string['fullscreenwidthdesc'] = 'Establece el ancho máximo de la pantalla en modo pantalla completa / zoom.';

$string['standardscreenwidth'] = 'Ancho estandar de la pantalla.';
$string['standardscreenwidthdesc'] = 'Establece el ancho de la pantalla del tema en modo estandar / no zoom.';

$string['narrow'] = '50%';
$string['standard'] = '80%';

$string['messagesiconscolor'] = 'Color de los iconos de notificaciones';
$string['messagesiconscolordesc'] = 'Establece el color de los iconos de notificaciones con escala de grises. 0% = negro / 100% = blanco';

// Help Links ******************************************************.
$string['headernavbarhelpheading'] = 'Enlace de Ayuda';
$string['headernavbarhelpheadingdesc'] = 'Opciones para el item de ayuda en el menú.';

$string['helpmenulink'] = 'Enlace a ayuda';
$string['helpmenulinkdesc'] = 'Añade un enlace a un sistema de ayuda externo.';

$string['helpmenutitle'] = 'Título del enlace de Ayuda';
$string['helpmenutitledesc'] = 'El título del enlace de ayuda que se mostrará en el menú.';

$string['helpmenutarget'] = 'Ventana objetivo del enlace de Ayuda';
$string['helpmenutargetdesc'] = 'Abre el enlace de Ayuda en una nueva ventana o en la misma.';

$string['targetnewwindow'] = 'Nueva ventana';
$string['targetsamewindow'] = 'Misma ventana';

$string['hideinforum'] = 'Oculta los menus de Ayuda y herramientas en los foros';
$string['hideinforumdesc'] = 'En pantallas pequeñas, los menús adicionales pueden desbordar la pantalla. Activa esta opción para ocultar los menús de Ayuda y Herramientas.';


// Courses Overlay *************************************************.
$string['rendereroverlaycolor'] = 'Color superpuesto';
$string['rendereroverlaycolordesc'] = 'El color superpuesto sobre el bloque de curso cuando "Bloques Superpuestos" está seleccionado.';

$string['rendereroverlayfontcolor'] = 'Color de texto en la superposición';
$string['rendereroverlayfontcolordesc'] = 'El color de la fuente cuando pasa el cursor por encima del bloque del curso y "Bloques Superpuestos" está activado.';

$string['rendereroverlaytitlecolor'] = 'Color del titulo del curso';
$string['rendereroverlaytitlecolordesc'] = 'Color del titulo del curso.';

$string['frontpagerendererdefaultimage'] = 'Imagen por defecto para los cursos';
$string['frontpagerendererdefaultimagedesc'] = 'Sube la imagen que se utilizará por defecto en los cursos que no dispongan de ella.  (solo se aplica en el estilo "Bloques Superpuestos")';

$string['tilesshowcontacts'] = 'Mostrar los contactos del curso';
$string['tilesshowcontactsdesc'] = 'Muestra / Oculta los contactos del curso.';

$string['tilesbordercolor'] = 'Color del borde de los bloques de cursos';
$string['tilesbordercolordesc'] = 'establece el color del borde de los bloques de cursos.';

$string['tilescontactstitle'] = 'Mostrar el rol de los contactos del curso';
$string['tilescontactstitledesc'] = 'Muestra / Oculta el rol del contacto en el curso. Si no se muestra, se mostrará una imagen para cada contacto.';

$string['tilesshowallcontacts'] = 'Mostrar todos los contactos del curso';
$string['tilesshowallcontactsdesc'] = 'Muestra todos los contactos del curso o solo el principal.';

$string['course'] = 'Curso';


// Alerts **********************************************************.
// Alert message if acting as other role.
$string['actingasrole'] = 'Estás actuando actualmente como un papel diferente';

// Alert Hidden Course.
$string['alerthiddencourse'] = 'Advertencia del curso oculto';
$string['alerthiddencoursedesc'] = 'Mostrar alerta en la página del curso si está oculta';

$string['alerthiddencoursetext-1'] = 'Este curso está oculto y no puede ser accedido por los estudiantes. ';
$string['alerthiddencoursetext-2'] = 'Haga clic aquí para actualizar la configuración';

// Alert Box Enable.
$string['enablealert'] = 'Activar Alerta {$a}';
$string['enablealertdesc'] = 'Activar la alerta {$a}.';

// Alert Box Generic Strings.
$string['alerttype'] = 'Tipo de Alerta';
$string['alerttypedesc'] = 'Selecciona el tipo de alerta: información (azul), aviso (amarillo) o anuncio (verde)';

$string['alerttext'] = 'Texto de la Alerta';
$string['alerttextdesc'] = 'Introduce el texto a mostrar en la alerta.';

$string['enablealerts'] = 'Activar alertas';
$string['enablealertsdesc'] = 'Activa alertas del sitio en la parte superior de la página.';

$string['enablealertcoursepages'] = 'Activar Alertas en las páginas de los cursos';
$string['enablealertcoursepagesdesc'] = 'Activar las alertas también en las páginas de los cursos.';

$string['enablealertstriptags'] = 'Eliminar etiquetas HTML del texto de la Alerta';
$string['enablealertstriptagsdesc'] = 'Si se activa, se eliminarán las etiquetas HTML del texto. Si está desactivado, se permitirá introducir etiquetas HTML por ejemplo para añadir enlaces.';

$string['alertkeyvalue'] = 'Clave de identificación de la Alerta';
$string['alertkeyvalue_details'] = 'Esta clave identifica la alerta y permite el control de la eliminación de la alerta en la pantalla por parte del usuario. Si se cambia la clave, los usuarios volverán a ver la alerta de nuevo.';

$string['alertsettingscourse'] = 'Configuraciçon Alertas de cursos';

$string['alertsettingsgeneral'] = 'Configuración Alertas';
$string['alertsettings'] = 'Alerta {$a}';

$string['alertcount'] = 'Contador de Alertas';
$string['alertcountdesc'] = 'Número de alertas a crear a continuación.';

$string['alertsettingsheading'] = 'Personalizar las Alertas';
$string['alertdesc'] = 'Introduce y personaliza el texto de las Alertas mostradas en la parte superior de la pantalla. Es posible crear más de una para mostrarse a diferentes tipos de usuarios. También existe la opción de mostrar las alertas en la página inicial y en las páginas de los cursos.';

// Alerts Types.
$string['alertdisabled'] = 'Desactivada';
$string['alertdisabledesc'] = 'Desactiva esta alerta.';

$string['alertinfo'] = 'Info';
$string['alertinfodesc'] = 'Muestra un mensaje de información.';

$string['alertwarning'] = 'Aviso';
$string['alertwarningdesc'] = 'Muestra un mensaje de aviso.';

$string['alertannounce'] = 'Anuncio';
$string['alertannouncedesc'] = 'Muestra un mensaje de anuncio.';

$string['alertprofilefield'] = 'Campo de usuario personalizado Nombre=Valor (opcional)';
$string['alertprofilefielddesc'] = 'Añade una regla de acceso usando un campo de perfil personalizado. Ejemplo: usertype=alumno';

// Alert Access - Visibility.
$string['alertaccessglobal'] = 'Visible para todo el mundo';
$string['alertaccessusers'] = 'Visible para usuarios';
$string['alertaccessadmins'] = 'Visible para administradores';
$string['alertaccessprofile'] = 'Añadir restricción por campo personalizado';

$string['alertaccess'] = 'Visibilidad de la Alerta';
$string['alertaccessdesc'] = 'Establece la visibilidad de la Alerta según su tipo. Nota: Si se usa "Añadir restricción por campo personalizado" necesitrás añadir el campo personalizado de usuario.';

// Moodle/Adaptable version alert messages.
$string['beta'] = 'VERSION EN DESARROLLO. NO USAR EN SITIOS EN PRODUCCIÓN';
$string['deprecated'] = 'VERSIÓN DE MOODLE OBSOLETA. NO USAR ADAPTABLE EN ESTE SITIO';

// Alerts Colors****************************************************.
$string['alertcolorsheading'] = 'Personaliza las alertas de la parte superior.';
$string['alertcolorsheadingdesc'] = 'Personaliza el color y el icono de las alertas de la parte superior.';

$string['alertcolorinfo'] = 'Color Info';
$string['alertcolorinfodesc'] = 'Color del icono en la alerta tipo Info.';
$string['alertbackgroundcolorinfo'] = 'Color de fondo Info';
$string['alertbackgroundcolorinfodesc'] = 'Color de fondo de la alerta tipo Info.';
$string['alertbordercolorinfo'] = 'Color de borde Info';
$string['alertbordercolorinfodesc'] = 'Color del borde de la alerta tipo Info.';
$string['alerticoninfo'] = 'Icono Info';
$string['alerticoninfodesc'] = 'Establece el icono de <a href="http://fortawesome.github.io/Font-Awesome/icons/">Font Awesome</a> a usar en las alertas tipo Info. Introducir el nombre sin el prefijo fa-';

$string['alertcolorwarning'] = 'Color Aviso';
$string['alertcolorwarningdesc'] = 'Color del icono en la alerta tipo Aviso.';
$string['alertbackgroundcolorwarning'] = 'Color de fondo Aviso';
$string['alertbackgroundcolorwarningdesc'] = 'Color de fondo de la alerta tipo Aviso.';
$string['alertbordercolorwarning'] = 'Color del borde Aviso';
$string['alertbordercolorwarningdesc'] = 'Color del borde de la alerta tipo Aviso.';
$string['alerticonwarning'] = 'Icono Aviso';
$string['alerticonwarningdesc'] = 'Establece el icono de <a href="http://fortawesome.github.io/Font-Awesome/icons/">Font Awesome</a> a usar en las alertas tipo Aviso. Introducir el nombre sin el prefijo fa-';

$string['alertcolorsuccess'] = 'Color Anuncio';
$string['alertcolorsuccessdesc'] = 'Color del icono en la alerta tipo Anuncio.';
$string['alertbackgroundcolorsuccess'] = 'Color de fondo Anuncio';
$string['alertbackgroundcolorsuccessdesc'] = 'Color de fondo de la alerta tipo Aviso.';
$string['alertbordercolorsuccess'] = 'Color del borde Anuncio';
$string['alertbordercolorsuccessdesc'] = 'Color del borde de la alerta tipo Anuncio.';
$string['alerticonsuccess'] = 'Icono Anuncio';
$string['alerticonsuccessdesc'] = 'Establece el icono de <a href="http://fortawesome.github.io/Font-Awesome/icons/">Font Awesome Icon</a> a usar en las alertas tipo Anuncio. Introducir el nombre sin el prefijo fa-';

// Mobile **********************************************************.
$string['mobilesettings'] = 'Dispositivos móviles';
$string['mobilesettingsheading'] = 'Configura la plantilla para ser visualizada en dispositivos móviles.';

$string['layoutmobilesettings'] = 'Configuración móvil';
$string['layoutmobilesettingsdesc'] = 'Configuración especifica para dispositivos móviles.';

$string['hidealertsmobile'] = 'Ocultar Alertas';
$string['hidealertsmobiledesc'] = 'Oculta las alertas de la parte superior de la pantalla (si están activadas)';

$string['hidesocialmobile'] = 'Ocultar los iconos sociales';
$string['hidesocialmobiledesc'] = 'Oculta los iconos sociales (si están activados)';

$string['hideasnavmobile'] = 'Muestra / oculta la actividad o sección';
$string['hideasnavmobiledesc'] = 'Muestra u oculta la actividad o sección del curso en dispositivos móviles';

$string['mobilehidelogotitle'] = 'Muestra / oculta el logo o el título del sitio';
$string['mobilehidelogotitledesc'] = 'Muestra / oculta el logo o el título del sitio en la cabecera en dispositivos móviles.';

$string['hidelogotitle'] = 'Oculta ambos';
$string['showlogo'] = 'Muestra el Logo';
$string['showtitle'] = 'Muestra el título del sitio';

$string['hideheadermobile'] = 'Ocultar la cabecera';
$string['hideheadermobiledesc'] = 'Oculta la cabecera, logo, iconos sociales y búsqueda.';

$string['hidebreadcrumbmobile'] = 'Ocultar ruta de navegación';
$string['hidebreadcrumbmobiledesc'] = 'Ocultar la ruta de navegación.';

$string['hidepagefootermobile'] = 'Ocultar pie de página';
$string['hidepagefootermobiledesc'] = 'Oculta el pie de página.';

$string['hideslidermobile'] = 'Ocultar el pase de diapositivas';
$string['hideslidermobiledesc'] = 'Oculta el pase de diapositivas de la página inicial.';

$string['hidelogomobile'] = 'Ocultar logo';
$string['hidelogomobiledesc'] = 'Ocultar el logo de la cabecera.';

$string['hidecoursetitlemobile'] = 'Oculta el título del curso';
$string['hidecoursetitlemobiledesc'] = 'Oculta el título del curso y del sitio';

// Layout **********************************************************.
$string['layoutsettings'] = 'Disposición';
$string['layoutdesc'] = 'Configuración de la disposición por defecto.';
$string['layoutsettingsheading'] = 'Controlar las diferentes disposiciones de la plantilla.';

$string['blockside'] = 'Posición de los bloques';
$string['blocksidedesc'] = 'Mostrar los bloques en la columna derecha o izquierda.';

$string['rightblocks'] = 'Columna derecha';
$string['leftblocks'] = 'Columna izquierda';

$string['sidebarnotlogged'] = 'Mostrar el panel lateral a los visitantes';
$string['sidebarnotloggeddesc'] = 'Muestra el panel lateral con bloques cuando los usuarios no han entrado.';

$string['emoticonsize'] = 'Tamaño emoticonos';
$string['emoticonsizedesc'] = 'Establece el tamaño (ancho y alto) de los emoticonos.';

$string['infoiconcolor'] = 'Color del icono de ayuda';
$string['infoiconcolordesc'] = 'Establece el color del icono de ayuda usado en los formularios.';

$string['dangericoncolor'] = 'Color del icono de aviso';
$string['dangericoncolordesc'] = 'Establece el color del icono de aviso/peligro usado principalmente en los campos obligatorios.';

$string['toolsmenu2'] = '2º Menu Herramientas';
$string['toolsmenu2desc'] = 'Puedes configurar los items de los menús de herramientas. Cada línea consiste en: titulo del item, enlace  (opcional), indicador de función (opcional) y el código de idioma o una lista de códigos separada por comas (opcional, para mostrar el item dependiendo del idioma del usuario) separado por una linea vertical (|). También pueden añadirse sub-items usando guiones. Por ejemplo:
<pre>
Moodle foros|https://moodle.org
-Moodle soporte|https://moodle.org/support
-Moodle desarrollo|https://moodle.org/development
--Moodle Docs|http://docs.moodle.org|Moodle Docs
--Moodle Docs en español|http://docs.moodle.org/es|Documentación en español|es
Moodle.com|http://moodle.com/
</pre>';

$string['toolsmenulabel'] = 'Herramientas';
$string['toolsmenulabel2'] = 'Herramientas 2';
$string['events'] = 'Eventos';
$string['mysites'] = 'Mis Cursos';
$string['hiddencourses'] = 'Cursos ocultos';
$string['pastcourses'] = 'Cursos anteriores';
$string['people'] = 'Participantes';
$string['help'] = 'Ayuda';

$string['showfooterblocks'] = 'Mostrar bloques en el pie de página';
$string['showfooterblocksdesc'] = 'Mostrar / Ocultar los bloques personalizables del pie de página.';

$string['breadcrumbseparator'] = 'Separador de la ruta de navegación';
$string['breadcrumbseparatordesc'] = 'Establece el icono de <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank">Font Awesome</a>  usado como separador entre los ítems de la ruta de navegación. Introducir el nombre del icono SIN el prefijo fa-';

$string['breadcrumbhome'] = 'Inicio de la ruta de navegación';
$string['breadcrumbhomedesc'] = 'Muestra el inicio de la ruta de navegación como icono o texto.';

$string['breadcrumbhometext'] = 'Texto';
$string['breadcrumbhomeicon'] = 'Icono';

$string['mysitesexclude'] = 'Mostrar excluyendo los cursos ocultos';
$string['mysitesinclude'] = 'Mostrar incluyendo los cursos ocultos';
$string['mysitesdisabled'] = 'Desactivado';

$string['newstickercount'] = 'Número de barras de anuncios';
$string['newstickercountdesc'] = 'Define barras de anuncios con diferentes reglas de acceso para mostrarlas a diferentes tipos de usuarios.';

$string['tickertext'] = 'Texto de la barra de anuncios';
$string['tickertextdesc'] = 'Añade el texto a mostrar en la barra de anuncios en formato de lista. Ver el archivo <a href="adaptable/README.txt">README.txt</a> para más información.';

$string['newmenu1trigger'] = 'Palabra clave para el Menú desplegable superior';
$string['newmenu1triggerdesc'] = 'Establece una palabra clave para el Menú desplegable superior.';

$string['menusheading'] = 'Configurar Navegación para los menús de la parte superior de la cabecera';
$string['menustitledesc'] = 'Los menús de herramientas (en la barra de navegación) y los menús superiores (cabecera superior) pueden ser restringidos dependiendo de un campo personalizado del perfil de usuario (opcional). Los menús siguen el formato de los menús estándar de moodle:
<pre>
Moodle foros|https://moodle.org
-Moodle soporte|https://moodle.org/support
-Moodle desarrollo|https://moodle.org/development
--Moodle Docs|http://docs.moodle.org|Moodle Docs
--Moodle Docs en español|http://docs.moodle.org/de|Documentation en español|es
Moodle.com|http://moodle.com/
</pre>
';

$string['menusession'] = 'Almacenar los detalles de acceso en la sesión';
$string['menusessiondesc'] = 'Se aconseja activar esta opción para optimizar el rendimiento en caso de utilizar varios menús. Mantener desactivado para pruebas.';

$string['disablecustommenu'] = 'Desactivar el menú personalizado de Moodle';
$string['disablecustommenudesc'] = 'Desactiva el menú personalizado de Moodle en la barra de navegación (seguirá funcionando en otras plantillas instaladas)';

$string['menusessionttl'] = 'Tiempo (minutos) de almacenamiento del acceso a los menús por sesión';
$string['menusessionttldesc'] = 'Número de minutos antes de reiniciar la sesión.';


// Tool menus ******************************************************.
$string['newmenudesc'] = 'Configurar los enlaces mostrados bajo el menú de la cabecera superior.';
$string['newmenufield'] = 'Nombre del campo personalizado del perfíl Field Name=Value (opcional)';
$string['newmenufielddesc'] = 'Añade una regla de acceso usando un campo de perfil personalizado. Ejemplo: usertype=alumno';
$string['newmenurequirelogin'] = 'Requiere Entrar';
$string['newmenurequirelogindesc'] = 'Si está activado este menú será visible a los usuarios que autenticados';

$string['menusdesc'] = '';

$string['newmenu2trigger'] = 'Palabra clave para el Menú desplegable superior';
$string['newmenu2triggerdesc'] = 'Establece la palabra clave para el Menú desplegable superior.';

$string['enablemenus'] = 'Activar Menús';
$string['enablemenusdesc'] = 'Por motivos de rendimiento, se recomienda no activarlo si no se utilizan los menús.';

$string['menuslinkicon'] = "Icono del menú de enlaces";
$string['menuslinkicondesc'] = 'Selecciona el icono de <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">Font Awesome Icon</a> para el menú de enlaces.';

$string['disablemenuscoursepages'] = 'Desactivar menús en las páginas de los cursos';
$string['disablemenuscoursepagesdesc'] = 'Activando esta opción los menús solo se mostrarán en las páginas del sitio, la página inicial, el Área Personal, etc. y no se mostrará en las páginas de los cursos.';

$string['topmenufontsize'] = 'Tamaño fuente menú superior';
$string['topmenufontsizedesc'] = 'Establece el tamaño de la fuente usada en el menú superior.';

$string['menuuseroverride'] = 'Permitir la anulación por usuario';
$string['menuuseroverridedesc'] = 'Esta configuración controla el uso de los menús a través del perfil de usuario. Para usar esta función, se ha de crear un campo personalizado de usuario tipo lista con los siguientes valores:
<pre>1. Sitewide (mostrará el menú en todo el sitio)
2. Homepages Only (mostrará el menú solo en la página principal)
3. Hidden (no se mostrarán los menús)</pre>
NO debes utilizar la opción "Desactivar menús en las páginas de los cursos" junto a los campos de perfil. En su lugar, selecciona "Homepage Only".

Nota: Los usuarios deberán salir de moodle y volver a entrar para que los cambios hagan efecto. Puede indicarse una nota al crear el campo personalizado.';

$string['menuoverrideprofilefield'] = 'Nombre del campo personalizado';
$string['menuoverrideprofilefielddesc'] = 'El nombre del campo de perfil "list" usado por la anulación por usuario.';
$string['menuoverrideprofilefielddefault'] = 'topmenusettings';

$string['topmenuscount'] = 'Número de menús superiores';
$string['topmenuscountdesc'] = 'Establece el número de menús superiores que quieres añadir en la cabecera.';

$string['menusheadingvisibility'] = 'Configuración para la visibilidad del menú superior';
$string['menusheadingvisibilitydesc'] = 'La siguiente configuración permite controlar donde mostrar los menús y opcionalmente permitir a los usuarios su personalización.';

$string['newmenuheading'] = 'Menú superior';
$string['newmenu'] = 'Menú superior desplegable';
$string['newmenutitle'] = 'Título menú superior';
$string['newmenutitledesc'] = 'El título del menú aparecerá en la cabecera del sitio';
$string['newmenutitledefault'] = 'Menú';

$string['enabletoolsmenus'] = 'Activar menús de herramientas';
$string['enabletoolsmenusdesc'] = 'Por razones de rendimiento se recomienda dejarlo desactivado si no se utilizan.';

$string['toolsmenuheading'] = 'Menús herramientas (en la barra de navegación)';
$string['toolsmenuheadingdesc'] = 'Puedes configurar los enlaces a mostrar en el menú de herramientas (en la barra de navegación).
 este formato es similar al usado por los menús personalizados de moodle pero permite añadir los iconos Font Awesome:
<pre>
&lt;span class=&quot;fa fa-video-camera&quot;&gt;&lt;/span&gt; Graba pantalla|https://example.com|Grabar pantalla
&lt;span class=&quot;fa fa-picture-o&quot;&gt;&lt;/span&gt; Banco imágenes|https://example.com|Banco imágenes
&lt;span class=&quot;fa fa-clock-o&quot;&gt;&lt;/span&gt; Reloj|https://example.com|Reloj
</pre><br />';

$string['toolsmenuscount'] = 'Número de menús de herramientas';
$string['toolsmenuscountdesc'] = 'Establece el número de menús de herramientas de la barra de navegación.';

$string['toolsmenuheading'] = 'Tools Menu ';
$string['toolsmenu'] = 'Menú de herramientas desplegable';
$string['toolsmenudesc'] = 'Añade un menú desplegable en la barra de navegación.';
$string['toolsmenutitle'] = 'Título menú de herramientas';
$string['toolsmenutitledefault'] = 'Herramientas';
$string['toolsmenutitledesc'] = 'Añade el título a mostrar en la barra de navegación.';

$string['toolsmenulabel'] = 'Menú Herramientas';

$string['toolsmenufield'] = 'Nombre del campo personalizado del perfíl Field Name=Value (opcional)';
$string['toolsmenufielddesc'] = 'Añade una regla de acceso usando un campo de perfil personalizado. Ejemplo: usertype=student';


// Social settings *************************************************.
$string['socialsettings'] = 'Redes sociales';
$string['socialheading'] = 'Configuración de los iconos de redes sociales';
$string['socialtitledesc'] = '<pre>Puedes desactivar la búsqueda y activar los iconos de redes sociales en su lugar.
Para configurar los iconos, introduce una lista delimitada en el campo "Lista de iconos sociales".
El formato debe ser:

url|titulo|icono

Ejemplo:
<pre>
https://example.com/course/search.php|Buscar en Moodle|fa-search
https://facebook.com/|Facebook|fa-facebook-square
https://twitter.com/|Twitter|fa-twitter-square
https://instagram.com|Instagram|fa-instagram
https://example.com|Mi Web|fa-globe
</pre>
El listado de iconos puedes encontrarlo en: <a href="http://fortawesome.github.io/Font-Awesome/icons">http://fortawesome.github.io/Font-Awesome/icons</a>Font-Awesome</pre>';

$string['socialsize'] = 'Configurar tamaño de los iconos sociales';
$string['socialsizedesc'] = 'Para una mejor visualización, el tamaño debe ser 5px mayor que el tamaño deseado.';
$string['socialsizemobile'] = 'Configurar tamaño de los iconos sociales en móviles';
$string['socialsizemobiledesc'] = 'Para una mejor visualización, el tamaño debe ser 5px mayor que el tamaño deseado.';
$string['socialpaddingside'] = 'Separación lateral iconos';
$string['socialpaddingsidedesc'] = 'Ajusta la separación entre los iconos sociales.';
$string['socialpaddingtop'] = 'Ajustar la separación inferior';
$string['socialpaddingtopdesc'] = 'Ajustar la separación debajo los iconos sociales (altera la posición vertical). Valor minimo de 15px (contando el margen) mas este valor.';

$string['socialtarget'] = 'Formato de apertura de la ventana de enlaces sociales';
$string['socialtargetdesc'] = 'Abrir la red social en la misma venta o en una nueva';

$string['socialsearchicon'] = 'Buscar en Moodle';
$string['socialsearchicondesc'] = 'Muestra el cuadro de búsqueda al lado de los iconos sociales.';

$string['socialicondesc'] = 'Selecciona el icono de <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">Font Awesome Icons</a> por ejemplo: fa-facebook';

$string['socialiconlist'] = 'Lista de iconos sociales';
$string['socialiconlistdesc'] = 'Introducir una lista delimitada por comas para configurar los iconos de redes sociales que necesites usando el formato: url|titulo|icono';

// Analytics **********************************.
$string['analyticssettings'] = 'Analítica web';
$string['analyticssettingsheading'] = 'Configura Google Analytics y/o Matomo';
$string['analyticssettingsdesc'] = 'Puedes configurar Google Analytics y/o Matomo.';

// GA.
$string ['googleanalyticssettings'] = 'Google Analytics';
$string ['googleanalyticssettingsheading'] = 'Configura Google Analytics para tu sitio';
$string ['googleanalyticssettingsdesc'] = 'Puedes configurar multiple códigos para Google Analytics y configurarlos para diferentes campus personalizados de usuario.';

$string ['enableanalytics'] = 'Activar Google Analytics';
$string ['enableanalyticsdesc'] = 'Activar Google Analytics en el sitio.';

$string ['analyticstext'] = 'Analytics ID';
$string ['analyticstextdesc'] = 'Introducir la ID de Google Analytics.';

$string['analyticscount'] = 'Número de campos de perfil';
$string['analyticscountdesc'] = 'Número de campos personalizados a introducir en el área inferior.';

$string ['analyticsprofilefield'] = 'Nombre del campo personalizado del perfíl Field Name=Value (opcional)';
$string ['analyticsprofilefielddesc'] = 'Añade una regla de acceso usando un campo de perfil personalizado. Ejemplo: usertype=alumno';

$string ['anonymizega'] = 'Anonimiza la IP del usuario';
$string ['anonymizegadesc'] = 'Anonimiza la IP del usuario enviada a Google Analytics';

// Matomo (anteriormente Piwik).
$string['piwiksettings'] = 'Matomo';
$string['piwiksettingsheading'] = 'Configurar Matomo (anteriormente Piwik)';
$string['piwiksettingsdesc'] = 'Generar una URL limpia para el rastreo avanzado.';

$string['piwikenabled'] = 'Activar Matomo';
$string['piwikenableddesc'] = 'Activar el rastreo de Matomo para Moodle.';

$string['piwiksiteid'] = 'ID del sitio';
$string['piwiksiteiddesc'] = 'Introducir la ID del sitio.';

$string['piwikimagetrack'] = 'Rastreo de imagen';
$string['piwikimagetrackdesc'] = 'Activar el rastreo por imagen para navegadores con JavaScript desactivado.';

$string['piwiksiteurl'] = 'URL de Matomo';
$string['piwiksiteurldesc'] = 'Introducir la dirección del sitio Matomo sin http (o https) ni la barra invertida.';

$string['piwiktrackadmin'] = 'Rastreo de administradores';
$string['piwiktrackadmindesc'] = 'Activar el rastreo de administradores (no recommendado)';


// Custom CSS ******************************.
$string['customcss'] = 'CSS y JS personalizado';
$string['customcssdesc'] = 'Introduce las reglas CSS y código JS personalizadas.';

$string['customcssjssettings'] = 'CSS y JS personalizado';
$string['genericsettingsheading'] = 'Introduce tu propio código CSS y Javascript';
$string['genericsettingsdescription'] = 'Introduce tu propio código CSS y Javascript.';

$string['jssection'] = 'Javascript personalizado';
$string['jssectiondesc'] = 'Introduce el código Javascript personalizado.';

$string['jssectionrestricted'] = 'Código Javascript personalizado condicional';
$string['jssectionrestricteddesc'] = 'Introduce el código Javascript basado en el campo personalizado de texto. Este código sólo se ejecutará si se cumple la condición.';

$string['jssectionrestrictedprofilefield'] = 'Campo personalizado para Javascript';
$string['jssectionrestrictedprofilefielddesc'] = 'Introduce el campo personalizado de usuario para ejecutar el código de la parte superior. Por ejemplo: facultad=ingenieria';

$string['jssectionrestricteddashboardonly'] = 'Incluye código Javascript solo en la página del Área personal';
$string['jssectionrestricteddashboardonlydesc'] = 'Muestra el código Javascript solo en el Área Personal. Si no, se muestra en todo el sitio.';

// Cache definitions.
$string['cachedef_userdata'] = 'El tiempo para guardar una sesión de un usuario específico.';

// Activity and section navigation ********************************.
$string['nextactivity'] = 'Siguiente actividad';
$string['previousactivity'] = 'Actividad anterior';

$string['nextsection'] = 'Siguiente';
$string['previoussection'] = 'Anterior';

$string['maincoursepage'] = 'Página principal';
$string['jumpto'] = 'Saltar a ...';

// General ******************************************.
$string['hide'] = 'Ocultar';
$string['show'] = 'Mostrar';
$string['showdesktop'] = 'Mostrar solo en computadoras de sobremesa';

// Navbar Links menu *********************************.
$string['linksmenu'] = 'Menu enlaces';

// Navbar user menu *********************************.
$string['usermenu'] = 'User menu';

// Save / Discard button text *********************************.
$string['savebuttontext'] = 'Guardar cambios';
$string['discardbuttontext'] = 'Cancelar';

// Forum settings.
$string['settingsforumheading'] = 'Foro';
$string['forumheaderbackgroundcolor'] = 'Color de fondo de la cabecera de la entrada';
$string['forumheaderbackgroundcolordesc'] = 'Color de fondo de la cabecera de una entrada.';
$string['forumbodybackgroundcolor'] = 'Color de fondo del contenido de la entrada';
$string['forumbodybackgroundcolordesc'] = 'Color de fondo del contenido de una entrada.';

// Accessibility settings.
$string['focusborder'] = 'Color borde elemento enfocado';
$string['focusborderdesc'] = 'Color del borde del elemento enfocado. Añadir un color para mejorar la accesibilidad. Establecer el color a <i>transparent (en inglés)</i> para ocultar el efecto.';
$string['focusbackground'] = 'Color fondo elemento enfocado';
$string['focusbackgrounddesc'] = 'Color del fondo del elemento enfocado. Añadir un color para mejorar la accesibilidad. Establecer el color a <i>transparent (en inglés)</i> para ocultar el efecto.';

// Course page further information *********************.
// Activity display *********************************.
$string['answered'] = 'Contestada';
$string['attempted'] = 'Intentada';
$string['contributed'] = 'Aportada';
$string['draft'] = 'Sin publicar para los alumnos';
$string['due'] = 'Hecho {$a}';
$string['feedbackavailable'] = 'Realimentación disponible';
$string['notanswered'] = 'No contestada';
$string['notattempted'] = 'No intentada';
$string['notcontributed'] = 'No aportada';
$string['notsubmitted'] = 'No presentada';
$string['overdue'] = 'Atrasada';
$string['reopened'] = 'Reabierta';
$string['submitted'] = 'Presentada';

$string['xofyanswered'] = '{$a->completed} de {$a->participants} contestadas';
$string['xofyattempted'] = '{$a->completed} de {$a->participants} intentadas';
$string['xofycontributed'] = '{$a->completed} of {$a->participants} aportadas';
$string['xofysubmitted'] = '{$a->completed} de {$a->participants} presentadas';
$string['xungraded'] = '{$a} sin cualificar';

$string['coursesectionactivityfurtherinformation'] = 'Más información de la página del curso';
$string['coursesectionactivityfurtherinformationassign'] = 'Mostrar información de las Tareas';
$string['coursesectionactivityfurtherinformationassigndesc'] = 'Muestra el estado de las Tareas, como enviada, contestada, etc.  Para los profesores y administradores muestra además el número de tareas presentadas.';
$string['coursesectionactivityfurtherinformationquiz'] = 'Muestra información de los Cuestionarios';
$string['coursesectionactivityfurtherinformationquizdesc'] = 'Muestra información del estado de los cuestionarios como enviado, contestado, etc. Para profesores y administradores muestra además el número de cuestionarios enviados.';
$string['coursesectionactivityfurtherinformationchoice'] = 'Muestra información de las Consultas';
$string['coursesectionactivityfurtherinformationchoicedesc'] = 'Muestra información del estado de las consultas como enviada, contestada, etc. Para profesores y administradores muestra además el número de consultas enviadas.';
$string['coursesectionactivityfurtherinformationfeedback'] = 'Muestra información de las Retroalimentaciones';
$string['coursesectionactivityfurtherinformationfeedbackdesc'] = 'Muestra información del estado de las Retroalimentaciones, como enviadas, etc. Para profesores y administradores muestra además el número de retroalimentaciones enviadas.';
$string['coursesectionactivityfurtherinformationlesson'] = 'Muestra información de las Lecciones';
$string['coursesectionactivityfurtherinformationlessondesc'] = 'Muestra información del estado de las Lecciones, como enviada, etc. Para profesores y administradores muestra además el número de Lecciones enviadas.';
$string['coursesectionactivityfurtherinformationdata'] = 'Muestra información de las Bases de Datos';
$string['coursesectionactivityfurtherinformationdatadesc'] = 'Muestra información del estado de las Bases de Datos, como enviada, etc. Para profesores y administradores muestra además el número de Bases de Datos enviadas.';

// Activity display margins.
$string['coursesectionactivitymargintop'] = 'Margen superior';
$string['coursesectionactivitymargintopdesc'] = 'Margen superior entre actividades';
$string['coursesectionactivitymarginbottom'] = 'Margen inferior';
$string['coursesectionactivitymarginbottomdesc'] = 'Margen inferior entre actividades.';

// Properties.
$string['properties'] = 'Importar / Exportar Configuración';
$string['propertiessub'] = 'Configuración actual del tema';
$string['propertiesdesc'] = 'En esta sección puedes importar / exportar la configuración de Adaptable (propiedades) en formato JSON. También puedes ver la configuración actual.';
$string['propertiesproperty'] = 'Propiedad';
$string['propertiesvalue'] = 'Valor';
$string['propertiesexport'] = 'Exportar las propiedades en una cadena tipo JSON';
$string['propertiesreturn'] = 'Volver';
$string['putpropertiesheading'] = 'Importar configuración';
$string['putpropertiesname'] = 'Importar propiedades';
$string['putpropertiesdesc'] = 'Pegar la cadena JSON y pulsar \'Guardar cambios \'.  Atención!  No se validan los valores y realiza un \'Purgar todas las cachés\'.';
$string['putpropertyreport'] = 'Informe:';
$string['putpropertyversion'] = 'versión:';
$string['putpropertyproperties'] = 'Propiedades';
$string['putpropertyour'] = 'Nuestro';
$string['putpropertiesignorecti'] = 'Ignorando la configuración de las imagenes de titulo del curso.';
$string['putpropertiesreportfiles'] = 'Recuerda subir los siguientes archivos a su configuración:';
$string['putpropertiessettingsreport'] = 'Informe de configuración:';
$string['putpropertiesvalue'] = '->';
$string['putpropertiesfrom'] = 'desde';
$string['putpropertieschanged'] = 'Modificados:';
$string['putpropertiesunchanged'] = 'No modificados:';
$string['putpropertiesadded'] = 'Añadidos:';
$string['putpropertiesignored'] = 'Ignorados:';

// Privacy.
$string['privacy:metadata'] = 'Adaptable no almacena información personal de los usuarios.';

// Small screen appearance settings.
$string['settingssmallscreen'] = 'Apariencia de los bloques en pantallas reducidas';
$string['smallscreenshowsearchicon'] = 'Mostrar icono de búsqueda';
$string['smallscreenshowsearchicondesc'] = 'Mostrar el icono de búsqueda en pantallas reducidas.';
$string['smallscreenhidebreadcrumb'] = 'Ocultar la ruta de navegación';
$string['smallscreenhidebreadcrumbdesc'] = 'Ocultar la ruta de navegación en pantallas reducidas.';
$string['smallscreenhidesidebar'] = 'Ocultar el panel lateral';
$string['smallscreenhidesidebardesc'] = 'Ocultar el panel lateral en pantallas reducidas.';

// Adaptable Tabbed layout changes.
$string['tabbedlayoutheading'] = 'Diseño de pestañas';
$string['tabbedlayoutcoursepage'] = 'Diseño de la página de curso por pestañas';
$string['tabbedlayoutcoursepagedesc'] = 'Utiliza un diseño por pestañas para la página del curso. El contenido se mostrará en pestañas, con el contenido del curso en una pestaña y permitiendo distribuir el resto en otras pestañas. Usar este apartado para configurar el orden de las pestañas.';
$string['tabbedlayoutcoursepagelink'] = 'Enlace al diseño de la página del curso por pestañas';
$string['tabbedlayoutcoursepagelinkdesc'] = 'Mostrar una pestaña con un enlace a la página del curso.';
$string['tabbedlayoutcoursepagetabcolorselected'] = 'Color de la pestaña seleccionada';
$string['tabbedlayoutcoursepagetabcolorselecteddesc'] = 'Establece el color de fondo de la pestaña seleccionada.';
$string['tabbedlayoutcoursepagetabcolorunselected'] = 'Color de las pestañas no seleccionadas';
$string['tabbedlayoutcoursepagetabcolorunselecteddesc'] = 'Establece el color de fondo de la pestañas no seleccionadas.';
$string['tabbedlayoutcoursepagetabpersistencetime'] = 'Tiempo de permanencia de la pestaña seleccionada';
$string['tabbedlayoutcoursepagetabpersistencetimedesc'] = 'Tiempo de permanencia de la pestaña seleccionada después de un tiempo de inactividad. Establece el tiempo en minutos. Por ejemplo, introduce 30 para que la primera pestaña se seleccione después de 30 minutos de inactividad';
$string['tabbedlayoutdashboard'] = 'Diseño por pestañas del Tablero';
$string['tabbedlayoutdashboarddesc'] = 'Utiliza un diseño por pestañas para el Tablero. El contenido se mostrará en pestañas, con el contenido del curso en una pestaña y permitiendo distribuir el resto en otras pestañas. Usar este apartado para configurar el orden de las pestañas.';
$string['tabbedlayoutdashboardtabcolorselected'] = 'Color de la pestaña seleccionada para el Tablero';
$string['tabbedlayoutdashboardtabcolorselecteddesc'] = 'Establece el color de fondo de la pestaña seleccionada.';
$string['tabbedlayoutdashboardtabcolorunselected'] = 'Color de fondo de la pestañas no seleccionadas del Tablero';
$string['tabbedlayoutdashboardtabcolorunselecteddesc'] = 'Establece el color de fondo de las pestañas no seleccionadas del tablero.';
$string['tabbedlayoutdashboardtab1condition'] = 'Restricción por campo personalizado del perfíl de la pestaña 1 (opcional)';
$string['tabbedlayoutdashboardtab1conditiondesc'] = 'Añade una condición para mostrar la pestaña 1 utilizando un campo personalizado del perfíl. Ejemplo: mostrarpestaña1=true';
$string['tabbedlayoutdashboardtab2condition'] = 'Restricción por campo personalizado del perfíl de la pestaña 2 (opcional)';
$string['tabbedlayoutdashboardtab2conditiondesc'] = 'Añade una condición para mostrar la pestaña 2 utilizando un campo personalizado del perfíl. Ejemplo: mostrarpestaña2=true';

$string['tabbedlayouttablabelcourse'] = 'Contenido';
$string['tabbedlayouttablabelcourse1'] = 'Pestaña 1';
$string['tabbedlayouttablabelcourse2'] = 'Pestaña 2';
$string['tabbedlayouttablabeldashboard'] = 'Tablero';
$string['tabbedlayouttablabeldashboard1'] = 'Pestaña 1';
$string['tabbedlayouttablabeldashboard2'] = 'Pestaña 2';

$string['region-course-tab-one-a'] = 'Región 1 para las pestañas del curso';
$string['region-course-tab-two-a'] = 'Región 2 para las pestañas del curso';
$string['region-my-tab-one-a'] = 'Región 1 para las pestañas del Tablero';
$string['region-my-tab-two-a'] = 'Región 2 para las pestañas del Tablero';

// Number of course tiles in front page.
$string['frontpagenumbertiles'] = 'Número de bloques de curso por fila';
$string['frontpagenumbertilesdesc'] = 'Número de bloques a mostrar en una fila en el listado de cursos de la portada del sitio.';
$string['frontpagetiles1'] = '1 bloque';
$string['frontpagetiles2'] = '2 bloques';
$string['frontpagetiles3'] = '3 bloques';
$string['frontpagetiles4'] = '4 bloques';
$string['frontpagetiles6'] = '6 bloques';

// Edit settings.
$string['editsettingsbutton'] = 'Botón de edición ';
$string['editsettingsbuttondesc'] = 'Configurar que debe aparecer en la barra de navegación para editar. Ten en cuenta qu estas opciones no se aplican a todas las páginas que no disponen de configuración como el Tablero.';
$string['editsettingsbuttonshowcog'] = 'Muestra solo el icono de edición (rueda)';
$string['editsettingsbuttonshowbutton'] = 'Muestra solo el botón &quot;Activa la edición&quot;.';
$string['editsettingsbuttonshowcogandbutton'] = 'Muestra el botón y el icono. (utiliza más espacio en la barra de navegación)';
$string['displayeditingbuttontext'] = 'Editar el texto del botón';
$string['displayeditingbuttontextdesc'] = 'Muestra u oculta el texto del botón. NOTA: Solo se aplica al botón.';

// Login *******************************************************.
$string['loginsettings'] = 'Página de entrada';
$string['loginsettingsheading'] = 'Personalizar la página de entrada';
$string['logindesc'] = 'Personalizar la página de entrada añadiendo una imagen de fondo y testo en la parte superior e inferior.';

$string['loginbgimage'] = 'Imagen de fondo';
$string['loginbgimagedesc'] = 'Añade una imagen para el fondo de la página de entrada.';

$string['logintextboxtop'] = 'Texto de la parte superior';
$string['logintextboxtopdesc'] = 'Añade un texto personalizado en la parte superior del formulario de entrada.';

$string['logintextboxbottom'] = 'Texto de la parte inferior';
$string['logintextboxbottomdesc'] = 'Añade un texto personalizado en la parte inferior del formulario de entrada.';

$string['loginfooter'] = 'Login page footer';
$string['loginfooterdesc'] = 'Show the footer in the login page.';

// Pending
$string['loginblockbgcolor'] = 'Box background color';
$string['loginblockbgcolordesc'] = 'Set the box background color.';

$string['loginblocktextcolor'] = 'Box text color';
$string['loginblocktextcolordesc'] = 'Set the box text color.';

$string['loginblockbordercolor'] = 'Box border color';
$string['loginblockbordercolordesc'] = 'Set the box border color.';

$string['loginblockborderwidth'] = 'Box border width';
$string['loginblockborderwidthdesc'] = 'Set the box border width.';

$string['loginblockborderstyle'] = 'Box border style';
$string['loginblockborderstyledesc'] = 'Set the box border style.';

$string['loginblockborderradius'] = 'Box border radius';
$string['loginblockborderradiusdesc'] = 'Set the box border radius.';

$string['loginblockshadow'] = 'Box shadow';
$string['loginblockshadowdesc'] = 'Show/hide the box shadow.';

// User profile ********************************************************************.
$string['aboutme'] = 'Sobre mí';
$string['course'] = 'Curso';
$string['courses'] = 'Cursos';
$string['more'] = 'Más';

// User & user profile settings ***************************************************.
$string['usersettings'] = 'Usuario';
$string['usersettingsdesc'] = 'Establece la configuración para el usuario.';
$string['usersettingsheading'] = 'Controla parámetros del usuario';
$string['customcoursetitle'] = 'Título del curso personalizado';
$string['customcoursetitledesc'] = 'Nombre del campo de usuario personalizado para el título del curso.';
$string['customcoursesubtitle'] = 'Subítulo del curso personalizado';
$string['customcoursesubtitledesc'] = 'Nombre del campo de usuario personalizado para el subtítulo del curso.';

$string['customfields'] = 'Campos personalizados';
$string['enabletabbedprofile'] = 'Activa perfil por pestañas';
$string['enabletabbedprofiledesc'] = 'Activa la visualización del perfil de usuario por pestañas.';
$string['enabledtabbedprofileeditprofilelink'] = 'Activa el enlace &#34;Editar Perfil&#34;';
$string['enabledtabbedprofileeditprofilelinkdesc'] = 'Activa el enlace de &#34;Editar perfil&#34; en las pestañas. Nota: Solo se mostrará si el usuario tiene permisos para editar el perfil.';
$string['enabledtabbedprofileuserpreferenceslink'] = 'Activa el enlace de &#34;Preferencias&#34;';
$string['enabledtabbedprofileuserpreferenceslinkdesc'] = 'Activa el enlace &#34;Preferencias" en las pestañas.';

$string['usernodescription'] = 'El usuario no ha actualizado todavía su descripción.';
$string['usernointerests'] = 'El usuario no ha actualizado todavía sus intereses.';

// Category headers settings.
$string['categoryheaderssettings'] = 'Cabecera de categorías';
$string['categoryheaderssettingsdesc'] = 'Configura la cabecera de las categorias.';
$string['categoryheaderssettingsheading'] = 'Configura las cabeceras para una categoria y sus descendientes.';

$string['categoryhavecustomheader'] = 'Cabecera personalizada de categoría';
$string['categoryhavecustomheaderdesc'] = 'Selecciona el nivel superior de la categoría tendrá la cabecera personalizada. Para seleccionar o deseleccionar más de una categoría, usa la tecla &#39;Ctrl&#39;. Guardar y recargar la página para actualizar.  Nota: Las sub-categorías heredarán los valores seleccionados.';
$string['categoryheaderheader'] = 'Configuración de la categoría superior &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;.';
$string['categoryheaderheaderdesc'] = 'Configura la categoría superior &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;.';
$string['categoryheaderheaderdescchildren'] = 'Configura la categoría superior &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; y sus subcategorias &#39;{$a->children}&#39;.';
$string['categoryheaderbgimage'] = 'Imagen de fondo de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;';
$string['categoryheaderbgimagedesc'] = 'Establece la imagen de fondo de la categoría superior &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; en la cabecera. El tamaño minimo es de 1600px x 180px (1900px x 180px recomendado). La imagen cubrirá totalmente la cabecera.';
$string['categoryheaderbgimagedescchildren'] = 'Establece la imagen de fondo de la categoría superior &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; y sus descendientes &#39;{$a->children}&#39; en la cabecera. El tamaño minimo de la imagen es de 1600px x 180px (1900px x 180px recomendado). La imagen cubrirá totalmente la cabecera.';
$string['categoryheaderlogo'] = 'Logo de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;';
$string['categoryheaderlogodesc'] = 'Logo de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; en la cabecera.';
$string['categoryheaderlogodescchildren'] = 'Establece el logo de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; y sus descendientes &#39;{$a->children}&#39; en la cabecera. El tamaño recomendado es 200px x 80px.';
$string['categoryheadercustomtitle'] = 'Titulo de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;';
$string['categoryheadercustomtitledesc'] = 'Establece el título de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; en la cabecera. Si se deja en blanco se mostrará la configuración de &#39;sitetitle&#39; y &#39;sitetitletext&#39; cuando &#39;enableheading&#39; está desactivado. En las páginas que no sean de cursos, se reemplazará el titulo del sitio. En las páginas de cursos, aparecerá bajo el título como se indiquue en &#39;enableheading&#39;. En dispositivos móviles,cualquier &#39;título&#39; se mostrará dependiendo de &#39;hidecoursetitlemobile&#39;.';
$string['categoryheadercustomtitledescchildren'] = 'Establece el título de la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; y sus descendientes &#39;{$a->children}&#39; en la cabecera. Si se deja en blanco se mostrará la configuración de &#39;sitetitle&#39; y &#39;sitetitletext&#39; cuando &#39;enableheading&#39; esta desactivado. En las páginas que no sean de cursos, se reemplazará el titulo del sitio. En las páginas de cursos, aparecerá bajo el título como se indique en &#39;enableheading&#39;. En dispositivos móviles,cualquier &#39;título&#39; se mostrará dependiendo de &#39;hidecoursetitlemobile&#39;.';
$string['categoryheadercustomcss'] = 'CSS personalizado para la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;';
$string['categoryheadercustomcssdesc'] = 'Añade el CSS personalizado para la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39;. Si los estilos no aparecen aplicados, verifica el log de PHP.';
$string['categoryheadercustomcssdescchildren'] = 'Añade el CSS personalizado para la categoría &#39;{$a->name}&#39; con id &#39;{$a->id}&#39; y sus descendientes &#39;{$a->children}&#39;. Este parámetro generará el CSS para el selector del tema definido por &#39;.category-{$a->id}&#39; y por sus ids descendientes. Si los estilos no aparecen aplicados, verifica el log de PHP.';
$string['invalidcategorycss'] = 'Categoría inválida para el CSS personalizado de &#39;{$a->topcatname}&#39; con id &#39;{$a->topcatid}&#39;: &#39;{$a->css}&#39;.';
$string['invalidcategorygeneratedcss'] = 'CSS personalizado inválido para la categoría &#39;{$a->css}&#39;';

// Search button.
$string['settingssearchcolors'] = 'Botón de búsqueda';
$string['buttonsearchcolor'] = 'Color de fondo del botón de búsqueda';
$string['buttonsearchcolordesc'] = 'Establece el color de fondo del botón de búsqueda situado junto al a caja de busqueda.';
$string['buttonsearchhovercolor'] = 'Color de fondo del botón de búsqueda al pasar el cursor';
$string['buttonsearchhovercolordesc'] = 'Establece el color de fondo del botón de búsqueda al pasar el cursor.';
$string['buttonsearchtextcolor'] = 'Color del texto del botón de búsqueda';
$string['buttonsearchtextcolordesc'] = 'Set the text colour for the search button.';

// About.
$string['about'] = 'Información del tema y soporte';
$string['support'] = 'Para solicitar soporte, visita <a href="https://adaptable.ws" target="_blank">adaptable.ws</a>';
