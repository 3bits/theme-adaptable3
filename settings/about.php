<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme_adaptable
 * @copyright  2020-2021 Fernando Acedo (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

// About heading.
$temp = new admin_settingpage('theme_adaptable_about', get_string('settingsabout', 'theme_adaptable'));

if ($ADMIN->fulltree) {
    $output = '<h4><strong>Moodle version: </strong>'.$CFG->release.'</h4>'.
              '<h4><strong>Adaptable version:</strong> 3.1.0 (Build: '.get_config('theme_adaptable', 'version').')</h4>';

    $output .= '<br><h4>'.get_string('support', 'theme_adaptable').'</h4><br>';

    $output .= '<h3>Licenses</h3>';

    $output .= '<p><b>Adaptable is licensed under:</b><br>GPL v3 (GNU General Public License) - <a href="https://www.gnu.org/licenses" target="_blank">https://www.gnu.org/licenses</a></p>';

    $output .= '<p><b>Google Fonts are licensed under:</b><br>
    SIL Open Font License v1.1 - <a href="https://scripts.sil.org/OFL" target="_blank">https://scripts.sil.org/OFL</a><br>
    Apache 2 license - <a href="https://www.apache.org/licenses/LICENSE-2.0" target="_blank">https://www.apache.org/licenses/LICENSE-2.0</a> <br>
    The Ubuntu fonts use the Ubuntu Font License v1.0 - <a href="https://font.ubuntu.com/ufl/ubuntu-font-licence-1.0.txt" target="_blank">https://font.ubuntu.com/ufl/ubuntu-font-licence-1.0.txt</a></p>';

    $output .= '<p><b>The Font Awesome font (by Dave Gandy) is licensed under:</b><br>
    SIL Open Font License v1.1 - <a href="https://scripts.sil.org/OFL" target="_blank">https://scripts.sil.org/OFL</a><br>
    Font Awesome CSS, LESS, and SASS files are licensed under MIT License - <a href="https://opensource.org/licenses/mit-license.html" target="_blank">https://opensource.org/licenses/mit-license.html</a></p>';

    $output .= '<p><b>Twemoji emojis are licensed under:</b><br>
    Copyright 2020 Twitter, Inc and other contributors<br>
    Code licensed under the MIT License - <a href="https://opensource.org/licenses/mit-license.html" target="_blank">https://opensource.org/licenses/mit-license.html</a><br>
    Graphics licensed under CC-BY 4.0 - <a href="https://creativecommons.org/licenses/by/4.0" target="_blank">https://creativecommons.org/licenses/by/4.0</a></p>';

    $output .= '<br /><p><span style="font-weight: bolder;">Adaptable</span> is not affiliated with or endorsed by Moodle.</p>';

    $temp->add(new admin_setting_heading('theme_adaptable_about',
               get_string('about', 'theme_adaptable'),
               format_text($output, FORMAT_MARKDOWN)));
}

$ADMIN->add('theme_adaptable', $temp);
