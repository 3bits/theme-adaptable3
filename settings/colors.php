<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Colors settings
 *
 * @package    theme_adaptable
 * @copyright  2015-2021 Fernando Acedo (3-bits.com)
 * @copyright  2020-2021 3bits development team (3-bits.com)
 * @copyright  2015-2016 Jeremy Hopkins (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

    // Colors section.
    $temp = new admin_settingpage('theme_adaptable_color', get_string('colorsettings', 'theme_adaptable'));
    $temp->add(new admin_setting_heading('theme_adaptable_color', get_string('colorsettingsheading', 'theme_adaptable'),
                   format_text(get_string('colordesc', 'theme_adaptable'), FORMAT_MARKDOWN)));

    // Main colors heading.
    $name = 'theme_adaptable/settingsmaincolors';
    $heading = get_string('settingsmaincolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    // Site main color.
    $name = 'theme_adaptable/maincolor';
    $title = get_string('maincolor', 'theme_adaptable');
    $description = get_string('maincolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Site background color.
    $name = 'theme_adaptable/backcolor';
    $title = get_string('backcolor', 'theme_adaptable');
    $description = get_string('backcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Main region background color.
    $name = 'theme_adaptable/regionmaincolor';
    $title = get_string('regionmaincolor', 'theme_adaptable');
    $description = get_string('regionmaincolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Link color.
    $name = 'theme_adaptable/linkcolor';
    $title = get_string('linkcolor', 'theme_adaptable');
    $description = get_string('linkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#607d8b', $previewconfig);
    $temp->add($setting);

    // Link hover color.
    $name = 'theme_adaptable/linkhover';
    $title = get_string('linkhover', 'theme_adaptable');
    $description = get_string('linkhoverdesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Accessibility.
    $name = 'theme_adaptable/settingsaccesscolors';
    $heading = get_string('settingsaccesscolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    // Focus border.
    $name = 'theme_adaptable/focusborder';
    $title = get_string('focusborder', 'theme_adaptable');
    $description = get_string('focusborderdesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Background color.
    $name = 'theme_adaptable/focusbackground';
    $title = get_string('focusbackground', 'theme_adaptable');
    $description = get_string('focusbackgrounddesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#d7ebfc', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Selection text color.
    $name = 'theme_adaptable/selectiontext';
    $title = get_string('selectiontext', 'theme_adaptable');
    $description = get_string('selectiontextdesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#000', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Selection background color.
    $name = 'theme_adaptable/selectionbackground';
    $title = get_string('selectionbackground', 'theme_adaptable');
    $description = get_string('selectionbackgrounddesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#33a3fe', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header colors heading.
    $name = 'theme_adaptable/settingsheadercolors';
    $heading = get_string('settingsheadercolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    // Loading bar color.
    $name = 'theme_adaptable/loadingcolor';
    $title = get_string('loadingcolor', 'theme_adaptable');
    $description = get_string('loadingcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#33a3fe', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Badge background color.
    $name = 'theme_adaptable/msgbadgecolor';
    $title = get_string('msgbadgecolor', 'theme_adaptable');
    $description = get_string('msgbadgecolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#e53935', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Badge text color.
    $name = 'theme_adaptable/msgbadgecolortext';
    $title = get_string('msgbadgecolortext', 'theme_adaptable');
    $description = get_string('msgbadgecolortextdesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Top header background color.
    $name = 'theme_adaptable/headerbkcolor';
    $title = get_string('headerbkcolor', 'theme_adaptable');
    $description = get_string('headerbkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Top header text color.
    $name = 'theme_adaptable/headertextcolor';
    $title = get_string('headertextcolor', 'theme_adaptable');
    $description = get_string('headertextcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#333', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Bottom header background color.
    $name = 'theme_adaptable/headerbkcolor2';
    $title = get_string('headerbkcolor2', 'theme_adaptable');
    $description = get_string('headerbkcolor2desc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Bottom header text color.
    $name = 'theme_adaptable/headertextcolor2';
    $title = get_string('headertextcolor2', 'theme_adaptable');
    $description = get_string('headertextcolor2desc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Menu colors heading.
    $name = 'theme_adaptable/settingsmenucolors';
    $heading = get_string('settingsmenucolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    // Main menu background color.
    $name = 'theme_adaptable/menubkcolor';
    $title = get_string('menubkcolor', 'theme_adaptable');
    $description = get_string('menubkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Main menu text color.
    $name = 'theme_adaptable/menufontcolor';
    $title = get_string('menufontcolor', 'theme_adaptable');
    $description = get_string('menufontcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#222', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Main menu hover color.
    $name = 'theme_adaptable/menuhovercolor';
    $title = get_string('menuhovercolor', 'theme_adaptable');
    $description = get_string('menuhovercolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#33a3fe', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Main menu bottom border color.
    $name = 'theme_adaptable/menubordercolor';
    $title = get_string('menubordercolor', 'theme_adaptable');
    $description = get_string('menubordercolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#33a3fe', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Mobile colors heading.
    $name = 'theme_adaptable/settingsmobilecolors';
    $heading = get_string('settingsmobilecolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    // Mobile menu background color.
    $name = 'theme_adaptable/mobilemenubkcolor';
    $title = get_string('mobilemenubkcolor', 'theme_adaptable');
    $description = get_string('mobilemenubkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#f9f9f9', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Mobile sidebar tab background colour.
    $name = 'theme_adaptable/mobileslidebartabbkcolor';
    $title = get_string('mobileslidebartabbkcolor', 'theme_adaptable');
    $description = get_string('mobileslidebartabbkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#3a454b', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Mobile sidebar tab icon colour.
    $name = 'theme_adaptable/mobileslidebartabiconcolor';
    $title = get_string('mobileslidebartabiconcolor', 'theme_adaptable');
    $description = get_string('mobileslidebartabiconcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#000', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Overlay tiles colors heading.
    $name = 'theme_adaptable/settingsoverlaycolors';
    $heading = get_string('settingsoverlaycolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    $name = 'theme_adaptable/rendereroverlaycolor';
    $title = get_string('rendereroverlaycolor', 'theme_adaptable');
    $description = get_string('rendereroverlaycolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#3a454b', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/rendereroverlayfontcolor';
    $title = get_string('rendereroverlayfontcolor', 'theme_adaptable');
    $description = get_string('rendereroverlayfontcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Tile Course title.
    $name = 'theme_adaptable/rendereroverlaytitlecolor';
    $title = get_string('rendereroverlaytitlecolor', 'theme_adaptable');
    $description = get_string('rendereroverlaytitlecolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/tilesbordercolor';
    $title = get_string('tilesbordercolor', 'theme_adaptable');
    $description = get_string('tilesbordercolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#3a454b', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/dividingline';
    $title = get_string('dividingline', 'theme_adaptable');
    $description = get_string('dividinglinedesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/dividingline2';
    $title = get_string('dividingline2', 'theme_adaptable');
    $description = get_string('dividingline2desc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Breadcrumb colors heading.
    $name = 'theme_adaptable/settingsbreadcrumbcolors';
    $heading = get_string('settingsbreadcrumbcolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    // Breadcrumb background color.
    $name = 'theme_adaptable/breadcrumb';
    $title = get_string('breadcrumbbackgroundcolor', 'theme_adaptable');
    $description = get_string('breadcrumbbackgroundcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#f5f5f5', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Breadcrumb text color.
    $name = 'theme_adaptable/breadcrumbtextcolor';
    $title = get_string('breadcrumbtextcolor', 'theme_adaptable');
    $description = get_string('breadcrumbtextcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#444', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Footer colors heading.
    $name = 'theme_adaptable/settingsfootercolors';
    $heading = get_string('settingsfootercolors', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    $name = 'theme_adaptable/footerbkcolor';
    $title = get_string('footerbkcolor', 'theme_adaptable');
    $description = get_string('footerbkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#424242', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/footertextcolor';
    $title = get_string('footertextcolor', 'theme_adaptable');
    $description = get_string('footertextcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/footertextcolor2';
    $title = get_string('footertextcolor2', 'theme_adaptable');
    $description = get_string('footertextcolor2desc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/footerlinkcolor';
    $title = get_string('footerlinkcolor', 'theme_adaptable');
    $description = get_string('footerlinkcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Forum colors.
    $name = 'theme_adaptable/settingsforumheading';
    $heading = get_string('settingsforumheading', 'theme_adaptable');
    $setting = new admin_setting_heading($name, $heading, '');
    $temp->add($setting);

    $name = 'theme_adaptable/forumheaderbackgroundcolor';
    $title = get_string('forumheaderbackgroundcolor', 'theme_adaptable');
    $description = get_string('forumheaderbackgroundcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_adaptable/forumbodybackgroundcolor';
    $title = get_string('forumbodybackgroundcolor', 'theme_adaptable');
    $description = get_string('forumbodybackgroundcolordesc', 'theme_adaptable');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add('theme_adaptable', $temp);
