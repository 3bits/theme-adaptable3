<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme_adaptable
 * @copyright  2015 Jeremy Hopkins (Coventry University)
 * @copyright  2015 Fernando Acedo (3-bits.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

$temp = new admin_settingpage('theme_adaptable_layout', get_string('layoutsettings', 'theme_adaptable'));
$temp->add(new admin_setting_heading('theme_adaptable_layout', get_string('layoutsettingsheading', 'theme_adaptable'), format_text(get_string('layoutdesc', 'theme_adaptable'), FORMAT_MARKDOWN)));

// Adaptable Tabbed layout changes.
$name = 'theme_adaptable/tabbedlayoutheading';
$heading = get_string('tabbedlayoutheading', 'theme_adaptable');
$setting = new admin_setting_heading($name, $heading, '');
$temp->add($setting);

// Course page tabbed layout.
$name = 'theme_adaptable/tabbedlayoutcoursepage';
$title = get_string('tabbedlayoutcoursepage', 'theme_adaptable');
$description = get_string('tabbedlayoutcoursepagedesc', 'theme_adaptable');
$default = $tabbedlayoutdefaultscourse[0];
$choices = $tabbedlayoutdefaultscourse;
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Have a link back to the course page in the course tabs.
$name = 'theme_adaptable/tabbedlayoutcoursepagelink';
$title = get_string('tabbedlayoutcoursepagelink', 'theme_adaptable');
$description = get_string('tabbedlayoutcoursepagelinkdesc', 'theme_adaptable');
$default = false;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$temp->add($setting);

// Course page tab colour selected.
$name = 'theme_adaptable/tabbedlayoutcoursepagetabcolorselected';
$title = get_string('tabbedlayoutcoursepagetabcolorselected', 'theme_adaptable');
$description = get_string('tabbedlayoutcoursepagetabcolorselecteddesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#06c', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course page tab colour unselected.
$name = 'theme_adaptable/tabbedlayoutcoursepagetabcolorunselected';
$title = get_string('tabbedlayoutcoursepagetabcolorunselected', 'theme_adaptable');
$description = get_string('tabbedlayoutcoursepagetabcolorunselecteddesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#eee', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course home page tab persistence time.
$name = 'theme_adaptable/tabbedlayoutcoursepagetabpersistencetime';
$title = get_string('tabbedlayoutcoursepagetabpersistencetime', 'theme_adaptable');
$description = get_string('tabbedlayoutcoursepagetabpersistencetimedesc', 'theme_adaptable');
$setting = new admin_setting_configtext($name, $title, $description, '30', PARAM_INT);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Dashboard page tabbed layout.
$name = 'theme_adaptable/tabbedlayoutdashboard';
$title = get_string('tabbedlayoutdashboard', 'theme_adaptable');
$description = get_string('tabbedlayoutdashboarddesc', 'theme_adaptable');
$default = $tabbedlayoutdefaultsdashboard[0];
$choices = $tabbedlayoutdefaultsdashboard;
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Dashboard page tab colour selected.
$name = 'theme_adaptable/tabbedlayoutdashboardcolorselected';
$title = get_string('tabbedlayoutdashboardtabcolorselected', 'theme_adaptable');
$description = get_string('tabbedlayoutdashboardtabcolorselecteddesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#06c', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Dashboard page tab colour unselected.
$name = 'theme_adaptable/tabbedlayoutdashboardcolorunselected';
$title = get_string('tabbedlayoutdashboardtabcolorunselected', 'theme_adaptable');
$description = get_string('tabbedlayoutdashboardtabcolorunselecteddesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#eee', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/tabbedlayoutdashboardtab1condition';
$title = get_string('tabbedlayoutdashboardtab1condition', 'theme_adaptable');
$description = get_string('tabbedlayoutdashboardtab1conditiondesc', 'theme_adaptable');
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW, '');
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/tabbedlayoutdashboardtab2condition';
$title = get_string('tabbedlayoutdashboardtab2condition', 'theme_adaptable');
$description = get_string('tabbedlayoutdashboardtab2conditiondesc', 'theme_adaptable');
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW, '');
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$ADMIN->add('theme_adaptable', $temp);
