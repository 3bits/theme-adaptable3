<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme_adaptable
 * @copyright  2015-2021 Fernando Acedo (3-bits.com)
 * @copyright  2020-2021 3bits development team (3-bits.com)
 * @copyright  2015 Jeremy Hopkins (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

// Course Formats.
$temp = new admin_settingpage('theme_adaptable_course', get_string('coursesettings', 'theme_adaptable'));
$temp->add(new admin_setting_heading('theme_adaptable_course', get_string('coursesettingsheading', 'theme_adaptable'), format_text(get_string('coursesettingsdesc', 'theme_adaptable'), FORMAT_MARKDOWN)));

// Show Your progress string in the top of the course.
$name = 'theme_adaptable/showyourprogress';
$title = get_string('showyourprogress', 'theme_adaptable');
$description = get_string('showyourprogressdesc', 'theme_adaptable');
$radchoices = array(
    'none' => get_string('hide', 'theme_adaptable'),
    'inline' => get_string('show', 'theme_adaptable'),
);
$setting = new admin_setting_configselect($name, $title, $description, '', $radchoices);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Activity end block region.
$temp->add(new admin_setting_heading('theme_adaptable_activity_bottom_heading',
    get_string('coursepageactivitybottomblockregionheading', 'theme_adaptable'),
    format_text(get_string('coursepageactivitybottomblockregionheadingdesc', 'theme_adaptable'), FORMAT_MARKDOWN)));
$name = 'theme_adaptable/coursepageblockactivitybottomenabled';
$title = get_string('coursepageblockactivitybottomenabled', 'theme_adaptable');
$description = get_string('coursepageblockactivitybottomenableddesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course block layout settings.
get_string('coursepageblockregionsettings', 'theme_adaptable');
$temp->add(new admin_setting_heading('theme_adaptable_heading', get_string('coursepageblocklayoutbuilder', 'theme_adaptable'),
        format_text(get_string('coursepageblocklayoutbuilderdesc', 'theme_adaptable'), FORMAT_MARKDOWN)));

// Course page top / bottom block regions enabled.
$name = 'theme_adaptable/coursepageblocksenabled';
$title = get_string('coursepageblocksenabled', 'theme_adaptable');
$description = get_string('coursepageblocksenableddesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// TOP LAYOUT *********************************************************************************************************************************.
// Course page top block region builder.
$noregions = 4; // Number of block regions defined in config.php.
$totalblocks = 0;
$imgpath = $CFG->wwwroot.'/theme/adaptable/pix/layout-builder/';
$imgblder = '';

$settingname = 'coursepageblocklayouttoprow';
if (!isset($PAGE->theme->settings->$settingname)) {
    $PAGE->theme->settings->$settingname = '0-0-0-0';
}

if ($PAGE->theme->settings->$settingname != '0-0-0-0') {
    $imgblder .= '<img src="' . $imgpath . $PAGE->theme->settings->$settingname . '.png' . '" style="padding-top: 5px">';
}

$vals = explode('-', $PAGE->theme->settings->$settingname);
foreach ($vals as $val) {
    if ($val > 0) {
        $totalblocks ++;
    }
}

$checkcountcolor = '#00695c';
if ($totalblocks > $noregions) {
    $mktcountcolor = '#d7542a';
}

// Top region.
$temp->add(new admin_setting_heading('theme_adaptable/coursepageblocklayouttop', "<h4>" . get_string('coursepageblocklayouttop', 'theme_adaptable') . "</h4>", ''));

$mktcountmsg = '<span style="color: ' . $checkcountcolor . '; margin-bottom: 20px; text-align:center;">';
$mktcountmsg .= get_string('layoutcount1', 'theme_adaptable') . '<strong>' . $noregions . '</strong>';
$mktcountmsg .= get_string('layoutcount2', 'theme_adaptable') . '<strong>' . $totalblocks . '/' . $noregions . '</strong></span>.';

// Check layout.
$temp->add(new admin_setting_heading('theme_adaptable_courselayouttopblockscount', '', $mktcountmsg));
$temp->add(new admin_setting_heading('theme_adaptable_courselayouttopbuilder', '', $imgblder . "<br><br><br><br>"));

// Course page Block Top Region Row.
$name = 'theme_adaptable/coursepageblocklayouttoprow';
$title = get_string('coursepageblocklayouttoprow', 'theme_adaptable');
$description = get_string('coursepageblocklayouttoprowdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, $bootstrap12defaults[0], $bootstrap12);
$temp->add($setting);


// BOTTOM LAYOUT *********************************************************************************************************************************.
// Course page bottom  block region builder.
$noregions = 4; // Number of block regions defined in config.php.
$totalblocks = 0;
$imgpath = $CFG->wwwroot.'/theme/adaptable/pix/layout-builder/';
$imgblder = '';
$settingname = 'coursepageblocklayoutbottomrow';

if (!isset($PAGE->theme->settings->$settingname)) {
    $PAGE->theme->settings->$settingname = '0-0-0-0';
}

if ($PAGE->theme->settings->$settingname != '0-0-0-0') {
    $imgblder .= '<img src="' . $imgpath . $PAGE->theme->settings->$settingname . '.png' . '" style="padding-top: 5px">';
}

$vals = explode('-', $PAGE->theme->settings->$settingname);
foreach ($vals as $val) {
    if ($val > 0) {
        $totalblocks ++;
    }
}

$checkcountcolor = '#00695c';
if ($totalblocks > $noregions) {
    $mktcountcolor = '#d7542a';
}

// Bottom region.
$temp->add(new admin_setting_heading('theme_adaptable/coursepageblocklayoutbottom', "<h4>" . get_string('coursepageblocklayoutbottom', 'theme_adaptable') . "</h4>", ''));

$mktcountmsg = '<span style="color: ' . $checkcountcolor . ';">';
$mktcountmsg .= get_string('layoutcount1', 'theme_adaptable') . '<strong>' . $noregions . '</strong>';
$mktcountmsg .= get_string('layoutcount2', 'theme_adaptable') . '<strong>' . $totalblocks . '/' . $noregions . '</strong></span>.';
$temp->add(new admin_setting_heading('theme_adaptable_courselayoutbottomblockscount', '', $mktcountmsg));
$temp->add(new admin_setting_heading('theme_adaptable_courselayoutbottombuilder', '', $imgblder . "<br><br>"));

// Bottom course region.
$name = 'theme_adaptable/coursepageblocklayoutbottomrow';
$title = get_string('coursepageblocklayoutbottomrow', 'theme_adaptable');
$description = get_string('coursepageblocklayoutbottomrowdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, $bootstrap12defaults[0], $bootstrap12);
$temp->add($setting);


// Topics / Weeks course format heading ****************************************************************.
$name = 'theme_adaptable/settingstopicsweeks';
$heading = get_string('settingstopicsweeks', 'theme_adaptable');
$setting = new admin_setting_heading($name, $heading, '');
$temp->add($setting);

// Current course section background color.
$name = 'theme_adaptable/coursesectionbgcolor';
$title = get_string('coursesectionbgcolor', 'theme_adaptable');
$description = get_string('coursesectionbgcolordesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Current course section header background color.
$name = 'theme_adaptable/currentcolor';
$title = get_string('currentcolor', 'theme_adaptable');
$description = get_string('currentcolordesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#d2f2ef', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section heading background color.
$name = 'theme_adaptable/coursesectionheaderbg';
$title = get_string('coursesectionheaderbg', 'theme_adaptable');
$description = get_string('coursesectionheaderbgdesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#eee', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section heading text color.
$name = 'theme_adaptable/sectionheadingcolor';
$title = get_string('sectionheadingcolor', 'theme_adaptable');
$description = get_string('sectionheadingcolordesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#333', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section header border bottom style.
$name = 'theme_adaptable/coursesectionheaderborderstyle';
$title = get_string('coursesectionheaderborderstyle', 'theme_adaptable');
$description = get_string('coursesectionheaderborderstyledesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, 'solid', $borderstyles);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section header border bottom color.
$name = 'theme_adaptable/coursesectionheaderbordercolor';
$title = get_string('coursesectionheaderbordercolor', 'theme_adaptable');
$description = get_string('coursesectionheaderbordercolordesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#f3f3f3', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section header border bottom width.
$name = 'theme_adaptable/coursesectionheaderborderwidth';
$title = get_string('coursesectionheaderborderwidth', 'theme_adaptable');
$description = get_string('coursesectionheaderborderwidthdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '2px', $from0to6px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section border radius.
$name = 'theme_adaptable/coursesectionheaderborderradiustop';
$title = get_string('coursesectionheaderborderradiustop', 'theme_adaptable');
$description = get_string('coursesectionheaderborderradiustopdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '0px', $from0to50px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section border radius.
$name = 'theme_adaptable/coursesectionheaderborderradiusbottom';
$title = get_string('coursesectionheaderborderradiusbottom', 'theme_adaptable');
$description = get_string('coursesectionheaderborderradiusbottomdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '0px', $from0to50px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section border style.
$name = 'theme_adaptable/coursesectionborderstyle';
$title = get_string('coursesectionborderstyle', 'theme_adaptable');
$description = get_string('coursesectionborderstyledesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, 'solid', $borderstyles);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section border width.
$name = 'theme_adaptable/coursesectionborderwidth';
$title = get_string('coursesectionborderwidth', 'theme_adaptable');
$description = get_string('coursesectionborderwidthdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '2px', $from0to6px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section border color.
$name = 'theme_adaptable/coursesectionbordercolor';
$title = get_string('coursesectionbordercolor', 'theme_adaptable');
$description = get_string('coursesectionbordercolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#eee', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course section border radius.
$name = 'theme_adaptable/coursesectionborderradius';
$title = get_string('coursesectionborderradius', 'theme_adaptable');
$description = get_string('coursesectionborderradiusdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '0px', $from0to50px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Activity display colours *************************************************************************************.

// Course Activity section heading.
$name = 'theme_adaptable/coursesectionactivitycolors';
$heading = get_string('coursesectionactivitycolors', 'theme_adaptable');
$setting = new admin_setting_heading($name, $heading, '');
$temp->add($setting);

// Use Adaptable icons.
$name = 'theme_adaptable/coursesectionactivityuseadaptableicons';
$title = get_string('coursesectionactivityuseadaptableicons', 'theme_adaptable');
$description = get_string('coursesectionactivityuseadaptableiconsdesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Default icon size.
$name = 'theme_adaptable/coursesectionactivityiconsize';
$title = get_string('coursesectionactivityiconsize', 'theme_adaptable');
$description = get_string('coursesectionactivityiconsizedesc', 'theme_adaptable');
$setting = new admin_setting_configtext($name, $title, $description, '24px');
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course Activity heading colour.
$name = 'theme_adaptable/coursesectionactivityheadingcolour';
$title = get_string('coursesectionactivityheadingcolour', 'theme_adaptable');
$description = get_string('coursesectionactivityheadingcolourdesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course Activity section bottom border width.
// This border was originally used all around an activity but changed to just the bottom.
$name = 'theme_adaptable/coursesectionactivityborderwidth';
$title = get_string('coursesectionactivityborderwidth', 'theme_adaptable');
$description = get_string('coursesectionactivityborderwidthdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '2px', $from0to6px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course Activity section bottom border style.
$name = 'theme_adaptable/coursesectionactivityborderstyle';
$title = get_string('coursesectionactivityborderstyle', 'theme_adaptable');
$description = get_string('coursesectionactivityborderstyledesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, 'solid', $borderstyles);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course Activity section bottom border colour.
$name = 'theme_adaptable/coursesectionactivitybordercolor';
$title = get_string('coursesectionactivitybordercolor', 'theme_adaptable');
$description = get_string('coursesectionactivitybordercolordesc', 'theme_adaptable');
$previewconfig = null;
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#ccc', $previewconfig);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course Activity section left border width.  Controls width of all left borders.
$name = 'theme_adaptable/coursesectionactivityleftborderwidth';
$title = get_string('coursesectionactivityleftborderwidth', 'theme_adaptable');
$description = get_string('coursesectionactivityleftborderwidthdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '3px', $from0to6px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Assign Activity display colours.
$name = 'theme_adaptable/coursesectionactivityassignleftbordercolor';
$title = get_string('coursesectionactivityassignleftbordercolor', 'theme_adaptable');
$description = get_string('coursesectionactivityassignleftbordercolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Assign Activity background colour.
$name = 'theme_adaptable/coursesectionactivityassignbgcolor';
$title = get_string('coursesectionactivityassignbgcolor', 'theme_adaptable');
$description = get_string('coursesectionactivityassignbgcolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Forum Activity display colours.
$name = 'theme_adaptable/coursesectionactivityforumleftbordercolor';
$title = get_string('coursesectionactivityforumleftbordercolor', 'theme_adaptable');
$description = get_string('coursesectionactivityforumleftbordercolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#909', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Forum Activity background colour.
$name = 'theme_adaptable/coursesectionactivityforumbgcolor';
$title = get_string('coursesectionactivityforumbgcolor', 'theme_adaptable');
$description = get_string('coursesectionactivityforumbgcolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Quiz Activity display colours.
$name = 'theme_adaptable/coursesectionactivityquizleftbordercolor';
$title = get_string('coursesectionactivityquizleftbordercolor', 'theme_adaptable');
$description = get_string('coursesectionactivityquizleftbordercolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#F33', null);
$temp->add($setting);

// Quiz Activity background colour.
$name = 'theme_adaptable/coursesectionactivityquizbgcolor';
$title = get_string('coursesectionactivityquizbgcolor', 'theme_adaptable');
$description = get_string('coursesectionactivityquizbgcolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', null);
$temp->add($setting);

// Top and bottom margin spacing between activities.
$name = 'theme_adaptable/coursesectionactivitymargintop';
$title = get_string('coursesectionactivitymargintop', 'theme_adaptable');
$description = get_string('coursesectionactivitymargintopdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '2px', $from0to12px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivitymarginbottom';
$title = get_string('coursesectionactivitymarginbottom', 'theme_adaptable');
$description = get_string('coursesectionactivitymarginbottomdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '2px', $from0to12px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// SocialWall course format heading.
$name = 'theme_adaptable/socialwall';
$heading = get_string('socialwall', 'theme_adaptable');
$setting = new admin_setting_heading($name, $heading, '');
$temp->add($setting);

// Socialwall background color.
$name = 'theme_adaptable/socialwallbackgroundcolor';
$title = get_string('socialwallbackgroundcolor', 'theme_adaptable');
$description = get_string('socialwallbackgroundcolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#fff', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Social Wall section border color.
$name = 'theme_adaptable/socialwallbordercolor';
$title = get_string('socialwallbordercolor', 'theme_adaptable');
$description = get_string('socialwallbordercolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#b9b9b9', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Social Wall section border style.
$name = 'theme_adaptable/socialwallbordertopstyle';
$title = get_string('socialwallbordertopstyle', 'theme_adaptable');
$description = get_string('socialwallbordertopstyledesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, 'solid', $borderstyles);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Social Wall section border width.
$name = 'theme_adaptable/socialwallborderwidth';
$title = get_string('socialwallborderwidth', 'theme_adaptable');
$description = get_string('socialwallborderwidthdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '2px', $from0to12px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Social Wall section border radius.
$name = 'theme_adaptable/socialwallsectionradius';
$title = get_string('socialwallsectionradius', 'theme_adaptable');
$description = get_string('socialwallsectionradiusdesc', 'theme_adaptable');
$setting = new admin_setting_configselect($name, $title, $description, '6px', $from0to12px);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Social Wall action link color.
$name = 'theme_adaptable/socialwallactionlinkcolor';
$title = get_string('socialwallactionlinkcolor', 'theme_adaptable');
$description = get_string('socialwallactionlinkcolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#51666c', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Social Wall hover link color.
$name = 'theme_adaptable/socialwallactionlinkhovercolor';
$title = get_string('socialwallactionlinkhovercolor', 'theme_adaptable');
$description = get_string('socialwallactionlinkhovercolordesc', 'theme_adaptable');
$setting = new admin_setting_configcolourpicker($name, $title, $description, '#0170ca', null);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// Course Activity Further Information section heading.
$name = 'theme_adaptable/coursesectionactivityfurtherinformation';
$heading = get_string('coursesectionactivityfurtherinformation', 'theme_adaptable');
$setting = new admin_setting_heading($name, $heading, '');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivityfurtherinformationassign';
$title = get_string('coursesectionactivityfurtherinformationassign', 'theme_adaptable');
$description = get_string('coursesectionactivityfurtherinformationassigndesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivityfurtherinformationquiz';
$title = get_string('coursesectionactivityfurtherinformationquiz', 'theme_adaptable');
$description = get_string('coursesectionactivityfurtherinformationquizdesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivityfurtherinformationchoice';
$title = get_string('coursesectionactivityfurtherinformationchoice', 'theme_adaptable');
$description = get_string('coursesectionactivityfurtherinformationchoicedesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivityfurtherinformationfeedback';
$title = get_string('coursesectionactivityfurtherinformationfeedback', 'theme_adaptable');
$description = get_string('coursesectionactivityfurtherinformationfeedbackdesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivityfurtherinformationlesson';
$title = get_string('coursesectionactivityfurtherinformationlesson', 'theme_adaptable');
$description = get_string('coursesectionactivityfurtherinformationlessondesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$name = 'theme_adaptable/coursesectionactivityfurtherinformationdata';
$title = get_string('coursesectionactivityfurtherinformationdata', 'theme_adaptable');
$description = get_string('coursesectionactivityfurtherinformationdatadesc', 'theme_adaptable');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

$ADMIN->add('theme_adaptable', $temp);
