Adaptable (The Original) - the most adaptable moodle theme
==========================================================

Version 3.1.1 (20211206)

Adaptable is a highly customizable responsive two column moodle theme that includes:

- Customizable fonts (Google Fonts)
- Customizable colors
- Customizable block styles (including FA icons)
- Customizable buttons
- Additional header navigation
- News Ticker
- Alternative jQuery slider
- Customizable front page course styles
- Additional customizable marketing blocks
- Additional layout settings for width, slider width, padding of
  various elements
- Social icons
- Mobile settings (customize how theme looks on mobile devices)
- Dismissible bootstrap alerts
- Option to add login form in header on front page
- Logo and Favicon uploader
- Modern emojis (thanks to Twemoji)
- Front Page layout builder
- Dashboard layout builder
- Course layout builder
- Activities status

In addition many fields (menus, news items, alerts and help links) can be targeted using custom profile fields, thus it is possible
to present different users with different navigation items and notices. It is also possible for individual users to customize where
they want top menu navigation to appear (disable, home pages only, site wide) using custom profile fields.

Adaptable has a lot of settings and may seem daunting at first, our advice is to simply install with the default settings and play
with it afterwards.

With a little time you should be able to setup an attractive Moodle site with a high degree of individuality without knowing any CSS.

This theme has been developed by 3bits elearning solutions:
Fernando Acedo (Lead Developer and Project Manager)
3bits development team


Change Log
----------
3.1.1 (20211206) Fix bug wnhen updating the theme. Spanish language pack updated and corrected.
3.1.0 (20211201) Some bugs fixed. UI/UX improved.
3.0.2 (20210923) Some bugs fixed and new improvements. Google Fonts updated.
3.0.1 (20210608) Fixed a minor bug
3.0.0 (20210601) Initial release for version 3.0


Versioning
----------
Adaptable is maintained under the Semantic Versioning (SemVer) guidelines as much as possible. Releases will be numbered with the
following format:

major.minor.patch

And following these guidelines:
- Breaking backward compatibility bumps the major (and resets the minor and patch)
- New additions without breaking backward compatibility bumps the minor (and resets the patch)
- Bug fixes and misc changes bumps the patch

For more information on SemVer, please visit https://semver.org.


Acknowledgments
---------------
Big thanks to all the volunteers that are collaborating and testing Adaptable continuously. We really appreciate your help and
support to develop the most adaptable theme for moodle.


Contributions
-------------
You are welcome to collaborate in the project. You can fix bugs, add new features or help in the translation to your language.
See CONTRIBUTING.txt for more information


Licenses
--------
Adaptable is licensed:
    PHP code under GPL v3 (GNU General Public License) - https://www.gnu.org/licenses
    CSS code under CC-BY 4.0 - https://creativecommons.org/licenses/by/4.0
    Graphics under CC-BY 4.0 - https://creativecommons.org/licenses/by/4.0

Google Fonts released under:
    SIL Open Font License v1.1 - https://scripts.sil.org/OFL
    Apache 2 license - https://www.apache.org/licenses/LICENSE-2.0
    The Ubuntu fonts use the Ubuntu Font License v1.0 - https://font.ubuntu.com/ufl/ubuntu-font-licence-1.0.txt

The Font Awesome font (by Dave Gandy) https://fontawesome.io) is licensed under:
    SIL Open Font License v1.1 - https://scripts.sil.org/OFL

Font Awesome CSS, LESS, and SASS files are licensed under:
    MIT License - https://opensource.org/licenses/mit-license.html

Emoji icons provided free by Twemoji (https://github.com/twitter/twemoji) released under:
    Code licensed under the MIT License - https://opensource.org/licenses/mit-license.html
    Graphics licensed under CC-BY 4.0 - https://creativecommons.org/licenses/by/4.0


Follow Us
---------
Twitter - https://twitter.com/adaptable_theme
Facebook - https://www.facebook.com/adaptable.theme

Modify it! - Improve it! - Share it!


Tips and tricks
---------------

* Ticker ************
You can add messages to the ticker using the editor. Enter the messages to display and press [enter]. Then convert the paragraphs
in a list.

The messages will be displayed in the ticker.

If you switch to the HTML editor, you should see something like this:
<ul>
    <li>Message One</li>
    <li>Message Two</li>
    <li>Message Three</li>
</ul>

If you still see the "No news items to display" message then verify the content is correct.
