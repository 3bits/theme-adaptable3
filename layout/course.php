<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *  Course-specific layout page
 *
 *  Includes course block region checking and formatting.
 *
 * @package    theme_adaptable
 * @copyright  2020-2021 Fernando Acedo (3-bits.com)
 * @copyright  2020-2021 3bits development team (3-bits.com)
 * @copyright  2017 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

// Include header.
require_once(dirname(__FILE__) . '/includes/header.php');

$left = $PAGE->theme->settings->blockside;

// If page is Grader report, override blockside setting to align left.
if (($PAGE->pagetype == "grade-report-grader-index") ||
    ($PAGE->bodyid == "page-grade-report-grader-index")) {
    $left = true;
}

$movesidebartofooter = false;

$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);

// Definition of block regions for top and bottom.  These are used in potentially retrieving any missing block regions (due to layout changes that may hide blocks).
$blocksarray = array (array('settingsname'  => 'coursepageblocklayouttoprow',
                      'classnamebeginswith' => 'course-top-'),
                      array('settingsname'  => 'coursepageblocklayoutbottomrow',
                      'classnamebeginswith' => 'course-bottom-')
);

if ($movesidebartofooter) {
//     Side post in the footer so don't adjust the layout but pretend it is not there.
    $regions = theme_adaptable_grid($left, false);
} else {
    $regions = theme_adaptable_grid($left, $hassidepost);
}
?>

<div class="container outercont">
    <?php
        echo $OUTPUT->page_navbar(false);
    ?>
    <div id="page-content" class="row<?php echo $regions['direction'];?>">
        <?php

        // If course page, display course top block region.
        if (!empty($PAGE->theme->settings->coursepageblocksenabled)) { ?>
            <div id="frontblockregion" class="container">
            <div class="row">
            <?php echo $OUTPUT->get_block_regions('coursepageblocklayouttoprow', 'course-top-'); ?>
            </div>
            </div>
        <?php
        }
        ?>

        <section id="region-main" class="<?php echo $regions['content'];?>">
            <?php
            echo $OUTPUT->get_course_alerts();
            echo $OUTPUT->course_content_header();
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();

// Check here if sidebar is configured to be in footer as we want to include the sidebar information in the main content.
            if ($movesidebartofooter == false) { ?>
        </section>
            <?php }

// Check if the block regions are disabled in settings.  If it is and there were any blocks assigned to those regions, they would obviously not display.
//This will allow to override the call to get_missing_block_regions to just display them all.
            $displayall = false;

            if (empty($PAGE->theme->settings->coursepageblocksenabled)) {
                $displayall = true;
            }

// Hide sidebar on mobile.
        $classes = '';
        if (!empty($PAGE->theme->settings->smallscreenhidesidebar)) {
            $classes = ' d-none d-md-block d-print-none ';
        }

        if ($movesidebartofooter == false) {
            if ($hassidepost) {
                echo $OUTPUT->blocks('side-post', $regions['blocks'] . $classes);
            }

            // Get any missing blocks from changing layout settings.  E.g. From 4-4-4-4 to 6-6-0-0, to recover
            // what was in the last 2 spans that are now 0.
            echo $OUTPUT->get_missing_block_regions($blocksarray, 'col-12', $displayall);
        }

        // If course page, display course bottom block region.
        if (!empty($PAGE->theme->settings->coursepageblocksenabled)) { ?>
            <div id="frontblockregion" class="container">
                <div class="row">
                    <?php echo $OUTPUT->get_block_regions('coursepageblocklayoutbottomrow', 'course-bottom-'); ?>
                </div>
            </div>
        <?php
        }

        if ($movesidebartofooter) {
            if ($hassidepost) {
                echo $OUTPUT->blocks('side-post', ' col-12 ' . $classes);
            }

            // Get any missing blocks from changing layout settings.  E.g. From 4-4-4-4 to 6-6-0-0, to recover
            // what was in the last 2 spans that are now 0.
            echo $OUTPUT->get_missing_block_regions($blocksarray, array(), $displayall);
        }

        if ($movesidebartofooter) { ?>
        </section>
        <?php } ?>
    </div>
</div>

<?php
// Include footer.
require_once(dirname(__FILE__) . '/includes/footer.php');

if (!empty($PAGE->theme->settings->tabbedlayoutcoursepagetabpersistencetime)) {
    $tabbedlayoutcoursepagetabpersistencetime = $PAGE->theme->settings->tabbedlayoutcoursepagetabpersistencetime;
} else {
    $tabbedlayoutcoursepagetabpersistencetime = 30;
}
if (!empty($PAGE->theme->settings->tabbedlayoutcoursepage)) {
    $PAGE->requires->js_call_amd('theme_adaptable/utils', 'init', array('currentpage' => $currentpage,
    'tabpersistencetime' => $tabbedlayoutcoursepagetabpersistencetime));

    echo '<noscript><style>label.coursetab { display: block !important; }</style><noscript>';
}
