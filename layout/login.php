<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package   theme_adaptable
 * @copyright 2015-2021 Fernando Acedo (3-bits.com)
 * @copyright 2020-2021 3bits development team (3-bits.com)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

// Include header.
global $PAGE, $OUTPUT;

require_once(dirname(__FILE__) . '/includes/header.php');

echo $OUTPUT->page_navbar(false);
?>

<div class="container outercont">
    <div id="page-content" class="row">
        <section id="region-main" class="col-12">
            <?php
            $logintextboxtop = $OUTPUT->get_setting('logintextboxtop', 'format_html');
            $logintextboxbottom = $OUTPUT->get_setting('logintextboxbottom', 'format_html');

            if (!empty($logintextboxtop)) {
                echo '<div class="row justify-content-center">
                <div class="col-xl-6 col-sm-8 ">
                <div class="card">
                <div class="card-body">'.$logintextboxtop.'</div></div></div></div>';
            }

            echo $OUTPUT->main_content();

            if (!empty($logintextboxbottom)) {
                echo '<div class="my-1 my-sm-5"></div>
                <div class="row justify-content-center">
                <div class="col-xl-6 col-sm-8 ">
                <div class="card">
                <div class="card-body">'.$logintextboxbottom.'</div></div></div></div>';
            }

            ?>
        </section>
    </div>
</div>

<?php
// Include footer.
if (!empty($PAGE->theme->settings->loginfooter)) {
    require_once(dirname(__FILE__) . '/includes/footer.php');
}
